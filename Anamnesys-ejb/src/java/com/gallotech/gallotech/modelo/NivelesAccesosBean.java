/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibNivelesAccesos;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAONivelesAccesos;
import com.gallotech.gallotech.encapsulacion.NivelesAccesos;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class NivelesAccesosBean extends DAOAbstractEJBModel<NivelesAccesos>{
    @Getter
    DAONivelesAccesos daoNivelesAccesos;
    public NivelesAccesosBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAONivelesAccesos.class);
        daoNivelesAccesos = (DAOHibNivelesAccesos) this.daoEntityClass;
    }
}






