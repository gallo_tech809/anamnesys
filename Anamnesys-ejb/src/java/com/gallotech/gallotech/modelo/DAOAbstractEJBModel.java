/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.genericos.GenericDAO;
import com.gallotech.gallotech.utileria.HibernateUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;

/**
 *
 * @author avasquez
 */
public abstract class DAOAbstractEJBModel<T>{
    @Getter
    protected GenericDAO daoEntityClass;
    @Getter
    private List<T> datasource;
    @Getter
    private Map<String, String> columnasOrdenarPor = new HashMap<>();

    public GenericDAO getDaoEntityClass() {
        return daoEntityClass;
    }
    
    public void setColumnasOrdenarPor(Map<String, String> parColumnasOrdenarPor) {
        if (parColumnasOrdenarPor == null) {
            columnasOrdenarPor = new HashMap<>();
        } else {
            columnasOrdenarPor = parColumnasOrdenarPor;
        }
        this.daoEntityClass.setColumnasOdenarPor(parColumnasOrdenarPor);
    }
    
    public void save(T objeto) throws HibernateException {
        try {
            HibernateUtil.getCurrentSession().beginTransaction();     
            daoEntityClass.save(objeto);
            HibernateUtil.getCurrentSession().getTransaction().commit();

        } catch (Exception e) {
             HibernateUtil.getCurrentSession().getTransaction().rollback();
        }
        
    }
    
    public void update(T objeto) {
       try {
            HibernateUtil.getCurrentSession().beginTransaction();     
            daoEntityClass.update(objeto);
            HibernateUtil.getCurrentSession().getTransaction().commit();

        } catch (Exception e) {
             HibernateUtil.getCurrentSession().getTransaction().rollback();
        }
    }
    
    public void delete(T objeto) {
        try {
            HibernateUtil.getCurrentSession().beginTransaction();     
            daoEntityClass.delete(objeto);
            HibernateUtil.getCurrentSession().getTransaction().commit();

        } catch (Exception e) {
             HibernateUtil.getCurrentSession().getTransaction().rollback();
        }
    }
    
    public void saveOrUpdate(T objeto){
        try {
            HibernateUtil.getCurrentSession().beginTransaction();     
            daoEntityClass.saveOrUpdate(objeto);
            HibernateUtil.getCurrentSession().getTransaction().commit();

        } catch (Exception e) {
             HibernateUtil.getCurrentSession().getTransaction().rollback();
        }
    }
    
    public void aplicarCondicionesAdicionales(Criteria parCriterioAplicarCondicion ) {
        daoEntityClass.aplicarCondicionesAdicionales(parCriterioAplicarCondicion);
    }
    
    public void modificarResultado(List<T> parRegistrosModificar){
    }
}
