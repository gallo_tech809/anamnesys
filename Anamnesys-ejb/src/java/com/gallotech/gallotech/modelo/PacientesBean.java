/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibPacientes;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOPacientes;
import com.gallotech.gallotech.encapsulacion.Pacientes;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class PacientesBean extends DAOAbstractEJBModel<Pacientes>{
    @Getter
    DAOPacientes daoPacientes;
    public PacientesBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOPacientes.class);
        daoPacientes = (DAOHibPacientes) this.daoEntityClass;
    }
}






