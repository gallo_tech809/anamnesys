/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibProcedimientoARS;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOProcedimientoARS;
import com.gallotech.gallotech.encapsulacion.ProcedimientoARS;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class ProcedimientoARSBean extends DAOAbstractEJBModel<ProcedimientoARS>{
    @Getter
    DAOProcedimientoARS daoProcedimientoARS;
    public ProcedimientoARSBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOProcedimientoARS.class);
        daoProcedimientoARS = (DAOHibProcedimientoARS) this.daoEntityClass;
    }
}






