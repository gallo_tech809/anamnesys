/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibEspecialidades;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOEspecialidades;
import com.gallotech.gallotech.encapsulacion.Especialidades;
import lombok.Getter;

/**
 *
 * @author HiraldoTran
 */
public class EspecialidadesBean extends DAOAbstractEJBModel<Especialidades> {
    @Getter
    DAOEspecialidades daoEspecialidades;
    public EspecialidadesBean(){
        this.daoEntityClass=new DAOHibernateFactory().instanciarDAOGenerico(DAOEspecialidades.class);
        daoEspecialidades=(DAOHibEspecialidades)this.daoEntityClass;
    }
}
