/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibServiciosMedicos;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOServiciosMedicos;
import com.gallotech.gallotech.encapsulacion.ServiciosMedicos;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class ServiciosMedicosBean extends DAOAbstractEJBModel<ServiciosMedicos>{
    @Getter
    DAOServiciosMedicos daoServiciosMedicos;
    public ServiciosMedicosBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOServiciosMedicos.class);
        daoServiciosMedicos = (DAOHibServiciosMedicos) this.daoEntityClass;
    }
}






