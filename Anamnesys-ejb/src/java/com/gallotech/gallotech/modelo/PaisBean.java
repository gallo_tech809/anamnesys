/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibPais;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOPais;
import com.gallotech.gallotech.encapsulacion.Pais;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class PaisBean extends DAOAbstractEJBModel<Pais>{
    @Getter
    DAOPais daoPais;
    public PaisBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOPais.class);
        daoPais = (DAOHibPais) this.daoEntityClass;
    }
}






