/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibEstudiosClinicos;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOEstudiosClinicos;
import com.gallotech.gallotech.encapsulacion.EstudiosClinicos;
import lombok.Getter;

/**
 *
 * @author HiraldoTran
 */
public class EstudiosClinicosBean extends DAOAbstractEJBModel<EstudiosClinicos> {
    @Getter
    DAOEstudiosClinicos daoEstudiosClinicos;
    public EstudiosClinicosBean(){
        this.daoEntityClass=new DAOHibernateFactory().instanciarDAOGenerico(DAOEstudiosClinicos.class);
        daoEstudiosClinicos=(DAOHibEstudiosClinicos)this.daoEntityClass;
    }
}
