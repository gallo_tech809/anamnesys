/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibEvaluaciones;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOEvaluaciones;
import com.gallotech.gallotech.encapsulacion.Evaluaciones;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class EvaluacionesBean extends DAOAbstractEJBModel<Evaluaciones>{
    @Getter
    DAOEvaluaciones daoEvaluaciones;
    public EvaluacionesBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOEvaluaciones.class);
        daoEvaluaciones = (DAOHibEvaluaciones) this.daoEntityClass;
    }
}






