/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibRecetaPaciente;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAORecetaPaciente;
import com.gallotech.gallotech.encapsulacion.RecetaPaciente;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class RecetaPacienteBean extends DAOAbstractEJBModel<RecetaPaciente>{
    @Getter
    DAORecetaPaciente daoRecetaPaciente;
    public RecetaPacienteBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAORecetaPaciente.class);
        daoRecetaPaciente = (DAOHibRecetaPaciente) this.daoEntityClass;
    }
}






