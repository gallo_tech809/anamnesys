/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibTipoEvaluacion;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOTipoEvaluacion;
import com.gallotech.gallotech.encapsulacion.TipoEvaluacion;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class TipoEvaluacionBean extends DAOAbstractEJBModel<TipoEvaluacion>{
    @Getter
    DAOTipoEvaluacion daoTipoEvaluacion;
    public TipoEvaluacionBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOTipoEvaluacion.class);
        daoTipoEvaluacion = (DAOHibTipoEvaluacion) this.daoEntityClass;
    }
}






