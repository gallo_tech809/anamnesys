/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibUsuarios;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOUsuarios;
import com.gallotech.gallotech.encapsulacion.Usuarios;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author alvasrey
 */
@Stateless
public class UsuarioBean extends DAOAbstractEJBModel<Usuarios>{
    @Getter
    DAOUsuarios daoUsuarios;
    public UsuarioBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOHibUsuarios.class);
        daoUsuarios = (DAOHibUsuarios) this.daoEntityClass;
    }
}
