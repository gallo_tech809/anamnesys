/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibAccesosUsuarios;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOAccesosUsuarios;
import com.gallotech.gallotech.encapsulacion.AccesosUsuarios;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author alvasrey
 */
@Stateless
public class AccesosUsuariosBean extends DAOAbstractEJBModel<AccesosUsuarios>{
    @Getter
    DAOAccesosUsuarios daoUsuarios;
    public AccesosUsuariosBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOHibAccesosUsuarios.class);
        daoUsuarios = (DAOHibAccesosUsuarios) this.daoEntityClass;
    }
}
