/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibProvincia;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOProvincia;
import com.gallotech.gallotech.encapsulacion.Provincia;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class ProvinciaBean extends DAOAbstractEJBModel<Provincia>{
    @Getter
    DAOProvincia daoProvincia;
    public ProvinciaBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOProvincia.class);
        daoProvincia = (DAOHibProvincia) this.daoEntityClass;
    }
}






