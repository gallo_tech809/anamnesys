/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibEnfermedades;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOEnfermedades;
import com.gallotech.gallotech.encapsulacion.Enfermedades;
import lombok.Getter;

/**
 *
 * @author HiraldoTran
 */
public class EnfermedadesBean extends DAOAbstractEJBModel<Enfermedades> {
    @Getter
    DAOEnfermedades daoEnfermedades;
    public EnfermedadesBean(){
        this.daoEntityClass=new DAOHibernateFactory().instanciarDAOGenerico(DAOEnfermedades.class);
        daoEnfermedades=(DAOHibEnfermedades)this.daoEntityClass;
    }
}
