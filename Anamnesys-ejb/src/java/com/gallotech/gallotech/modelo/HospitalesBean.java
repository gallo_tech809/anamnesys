/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibHospitales;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOHospitales;
import com.gallotech.gallotech.encapsulacion.Hospitales;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class HospitalesBean extends DAOAbstractEJBModel<Hospitales>{
    @Getter
    DAOHospitales daoHospitales;
    public HospitalesBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOHospitales.class);
        daoHospitales = (DAOHibHospitales) this.daoEntityClass;
    }
}






