/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibMedicamentos;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOMedicamentos;
import com.gallotech.gallotech.encapsulacion.Medicamentos;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class MedicamentosBean extends DAOAbstractEJBModel<Medicamentos>{
    @Getter
    DAOMedicamentos daoMedicamentos;
    public MedicamentosBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOMedicamentos.class);
        daoMedicamentos = (DAOHibMedicamentos) this.daoEntityClass;
    }
}






