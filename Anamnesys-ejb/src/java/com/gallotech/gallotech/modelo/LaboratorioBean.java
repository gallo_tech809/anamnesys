/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.modelo;

import com.gallotech.gallotech.dao.daohibernate.DAOHibLaboratorio;
import com.gallotech.gallotech.dao.daohibernate.DAOHibernateFactory;
import com.gallotech.gallotech.dao.interfaces.DAOLaboratorio;
import com.gallotech.gallotech.encapsulacion.Laboratorio;
import javax.ejb.Stateless;
import lombok.Getter;

/**
 *
 * @author Bayardo Mejia
 */

@Stateless
public class LaboratorioBean extends DAOAbstractEJBModel<Laboratorio>{
    @Getter
    DAOLaboratorio daoLaboratorio;
    public LaboratorioBean(){
        this.daoEntityClass = new DAOHibernateFactory().instanciarDAOGenerico(DAOLaboratorio.class);
        daoLaboratorio = (DAOHibLaboratorio) this.daoEntityClass;
    }
}






