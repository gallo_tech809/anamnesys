/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria.helpers;

import javax.inject.Named;
import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author jrozon
 */
@Named(value = "helperDePermisos")
@ManagedBean(name = "helperdepermisos")
@SessionScoped
public class HelperDePermisos implements Serializable {
    
    /**
     * Creates a new instance of HelperDePermisos
     */
    public HelperDePermisos() {
    }

    public Boolean puedeAccederSistema(String usuaio, short sistema) {
//        Usuarios usuario = getModUsuarios().buscarPorUsuarioYSistema(usuaio, sistema);
//        if (usuario != null) {
//            if (usuario.getEstatus().contentEquals("A")) {
//                return true;
//            }else
//            {
//                return false;
//            }
//        }
        return false;
    }

    public void agregarSistemaAccedido(short sistema) {
//        Sistemas sistemas = getModSistemas().buscarPorID(sistema);
//        FacesContext fc = FacesContext.getCurrentInstance();
//        Map<String, Object> sessionMap = fc.getExternalContext().getSessionMap();
//        List<Sistemas> sistemasAccedidos = (List<Sistemas>) sessionMap.get("sistemas");
//        if (sistemasAccedidos == null) {
//            sistemasAccedidos = new ArrayList<>();
//        }
//        sistemasAccedidos.add(sistemas);
//        sessionMap.put("sistemas", sistemasAccedidos);
    }

    public Boolean puedeAccederSistemaLogin() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, Object> sessionMap = fc.getExternalContext().getSessionMap();
        String user = (String) sessionMap.get("username");
        short codigoSistema = Short.parseShort(sessionMap.get("codigoSistema") + "");
        System.out.println("SISTEMA UTILIZADO : " + codigoSistema);
        return puedeAccederSistema(user, codigoSistema);

    }
     
}
