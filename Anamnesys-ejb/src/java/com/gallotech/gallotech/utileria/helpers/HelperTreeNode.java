/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria.helpers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @author avasquez
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class HelperTreeNode {
    private String datoAlmacenado;
    private String valorAlmacenado;
    private Object valoresAlmacenados;
    
}
