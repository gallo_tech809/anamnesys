/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import org.hibernate.service.ServiceRegistry;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Jmartinez
 */
//public class HibernateUtil {
//
//    private static final SessionFactory sessionFactory;
//    private static final Configuration configuration;
//    static {
//        
//        try {
//            
//            configuration = new AnnotationConfiguration();
//            configuration.configure();
//            configuration.setProperty(org.hibernate.cfg.Environment.SHOW_SQL, "true");
//            System.out.println(configuration.getProperty("name"));
//            // Create the SessionFactory from standard (hibernate.cfg.xml) 
//            // config file.
//            sessionFactory = configuration.buildSessionFactory();            
//            System.out.println("SE CREO LA SESSION");
//        } catch (Throwable ex) {
//            // Log the exception. 
//            ex.printStackTrace();
//            
//            System.err.println("Initial SessionFactory creation failed." + ex);
//            throw new ExceptionInInitializerError("Causa:"+ex.getCause()+"\n"+"Mensaje:"+ex.getMessage());
//        }
//    }
//    
//    public static SessionFactory getSessionFactory() { 
//        if ( sessionFactory.isClosed()){
//            System.out.println("sessionFactory ESTA CERRADA SERA ABIERTA");
//            sessionFactory.openSession();
//        }
//        return sessionFactory;
//    }
//}
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static Session hibernateSession;

    static {
        try {
            Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
            StandardServiceRegistryBuilder sb = new StandardServiceRegistryBuilder();
            sb.applySettings(cfg.getProperties());
            StandardServiceRegistry standardServiceRegistry = sb.build();
            sessionFactory = cfg.buildSessionFactory(standardServiceRegistry);
            hibernateSession = sessionFactory.openSession();

        } catch (Throwable th) {
            th.printStackTrace();
            System.err.println("Enitial SessionFactory creation failed" + th);
            throw new ExceptionInInitializerError(th);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static Session getCurrentSession(){
        //System.out.println("estado de la session (connected): "+hibernateSession.isConnected());
        //System.out.println("estado de la session (openned): "+hibernateSession.isOpen());
        if(!hibernateSession.isOpen() || !hibernateSession.isConnected()){
            System.out.println("session was closed");
            hibernateSession = sessionFactory.openSession();
        }
        
        return hibernateSession;
    }
    
//    public Session openSession(){
//        hibernateSession = sessionFactory.openSession();
//    }

}
