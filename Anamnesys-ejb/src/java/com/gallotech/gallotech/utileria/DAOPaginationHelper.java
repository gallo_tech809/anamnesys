/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria;

import com.gallotech.gallotech.modelo.DAOAbstractEJBModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * 
 * @author avasquez
 */ 
public class DAOPaginationHelper<T> extends LazyDataModel<T> {
    private List<T> datasource;
    private int indiceFilasAdicionales=0;
    private int totalFilasAdicionales=0;
    private List<T> filasAdicionalesInsertar;
    private Comparator ordenarDatosListaPor;
    private DAOAbstractEJBModel abstractModel;
    private Map<String, Object> columnasAplicaronFiltro;

    public DAOPaginationHelper() {
    }

    public DAOPaginationHelper(DAOAbstractEJBModel abstractModel) {
        this.abstractModel = abstractModel;
    }

    @Override
    public T getRowData(String rowKey) {
        System.out.println("getRowData sTRING : " + rowKey);
        for (T car : datasource) {
            if (car.equals(rowKey)) {
                return car;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(T car) {
        System.out.println("getRowData t : " + car);
        return car;
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        //        List<T> data = new ArrayList<>();
        int pageSizeOrigen;
        System.out.println("BOOM" + filters);
        this.columnasAplicaronFiltro = filters;

        try {
            System.out.println("filtros: " + filters);
            System.out.println(first + " pageSize : " + pageSize + this.filasAdicionalesInsertar);
            // Si existen filas adicionales que serán insertadas, entonces se debe tomar en cuenta, cual es el total
            // de filas para solo obtener de la base de datos el complemento. Ejemplo:
            // first = 1, pageSize = 10 y totalFilasInsertar = 5, entonces first debe ser = 6;
            pageSizeOrigen = pageSize;
            if ( first < this.totalFilasAdicionales ){
                pageSize = pageSize - this.totalFilasAdicionales;
            }
            if ( pageSize <= 0 )
            {
                if ( datasource != null )
                    datasource.clear();
                
                this.insertarFilasAdicionales(first, pageSizeOrigen);
                return datasource;
            }
            
            if (!filters.isEmpty()) {
                datasource = abstractModel.getDaoEntityClass().filtrarPorRango(filters, first, first + pageSize);
                int dataSize = abstractModel.getDaoEntityClass().countByQuery(filters, first, first + pageSize);
                this.setRowCount(dataSize);
            } else {
                datasource = abstractModel.getDaoEntityClass().buscarRango(first, first + pageSize);
                System.out.println("DAO ENTITY CLASS: " + abstractModel.getDaoEntityClass());
                System.out.println("datasource: "+datasource);
                int dataSize = abstractModel.getDaoEntityClass().count();
                this.setRowCount(dataSize);
            }
            this.insertarFilasAdicionales(first, pageSizeOrigen);
            
        } catch (Exception e) {
            System.out.println("Exception in load(): " + e.getMessage());
            e.printStackTrace();
        }
        abstractModel.modificarResultado(datasource);
        return datasource;
    }

    @Override
    public void setRowIndex(int rowIndex) {
        /*
         * The following is in ancestor (LazyDataModel):
         * this.rowIndex = rowIndex == -1 ? rowIndex : (rowIndex % pageSize);
         */
        if (rowIndex == -1 || getPageSize() == 0) {
            super.setRowIndex(-1);
        } else {
            super.setRowIndex(rowIndex % getPageSize());
        }
    }

    /***
     * Función para asignar las filas adicionales que serán insertadas en el DataSource
     * Cuando se ejecuten los métodos Load y getDataSourceFiltrado. Esto fue necesario
     * debido a que existe consultas que se desea mantener la funcionalidad del Pagination
     * y además poder insertar datos al resultado obtenido.
     * @param filasInsertar, Una lista con las filas que se desean insertar
     * @param ordenarListaPor , un Comparator, el cual contiene una clase que indica como serán ordenado los resultados.
     */
    public void setFilasAdicionales(List<T> filasInsertar, Comparator ordenarListaPor, int indice){
        this.filasAdicionalesInsertar = filasInsertar;
        this.ordenarDatosListaPor = ordenarListaPor;
        this.indiceFilasAdicionales = indice;
        if ( filasInsertar != null )
            this.totalFilasAdicionales = filasInsertar.size();
    }
    public int insertarFilasAdicionales( int parFilaInicial, int parTotalFilas){
        
        if ( parFilaInicial > this.indiceFilasAdicionales + 1)
            return 0;
       
        if ( this.filasAdicionalesInsertar != null && this.filasAdicionalesInsertar.size() > 0 ){
            if ( this.datasource == null )
            {
                this.datasource = new ArrayList<>();
            }
            
             if ( this.totalFilasAdicionales < parTotalFilas)
                {
                    this.datasource.addAll(this.indiceFilasAdicionales, this.filasAdicionalesInsertar);
                }else
                {
                    int indice= this.indiceFilasAdicionales;
                    for (T registro : this.filasAdicionalesInsertar) {
                        if ( ( indice + 1 ) > parTotalFilas )
                            break;
                        this.datasource.add(indice, registro);
                        indice ++;
                        
                    }
                }
        }
        
        if ( this.ordenarDatosListaPor != null ){
            Collections.sort(datasource, ordenarDatosListaPor);
        }
        
        this.setRowCount(this.getRowCount() + this.totalFilasAdicionales);
        return 0;
    }
    public List<T> getDataSourceFiltrado() {
        return abstractModel.getDaoEntityClass().filtrarPorColumnas(columnasAplicaronFiltro);
    }
}
