/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria;

import com.gallotech.gallotech.dao.daohibernate.DAOHibAccesosUsuarios;
import com.gallotech.gallotech.encapsulacion.AccesosUsuarios;
import com.gallotech.gallotech.encapsulacion.AccesosUsuariosPK;
import com.gallotech.gallotech.dao.interfaces.DAOAccesosUsuarios;

/**
 *
 * @author HiraldoTran
 */
public class Sitema_medico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AccesosUsuariosPK idAccesoUsuario = new AccesosUsuariosPK(1, 1);
        DAOAccesosUsuarios daoAccesosUsuarios = new DAOHibAccesosUsuarios();
        AccesosUsuarios resultado = daoAccesosUsuarios.buscarPorID(idAccesoUsuario);
        System.out.println("resultado: " + resultado.getOpcionMenu());
        
    }
    
}
