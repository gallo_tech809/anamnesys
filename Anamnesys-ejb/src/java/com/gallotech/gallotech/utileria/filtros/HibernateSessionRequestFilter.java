/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria.filtros;

import com.gallotech.gallotech.utileria.HibernateUtil;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;

/**
 *
 * @author avasquez
 */
public class HibernateSessionRequestFilter implements Filter {  
  
    private static Log log = LogFactory.getLog(HibernateSessionRequestFilter.class);  
  
    private SessionFactory sf;  
  
    @Override
    public void doFilter(ServletRequest request,  
                         ServletResponse response,  
                         FilterChain chain)  
            throws IOException, ServletException {  
        
         Session s = sf.openSession();
  
        try {  
           
            log.debug("Starting a database transaction");  
            s.beginTransaction();  
  
            // Call the next filter (continue request processing)  
            chain.doFilter(request, response);  
  
            // Commit and cleanup  
            log.debug("Committing the database transaction");  
            s.getTransaction().commit();  
  
        } catch (StaleObjectStateException staleEx) {  
            log.error("This interceptor does not implement optimistic concurrency control!");  
            log.error("Your application will not work until you add compensation actions!");  
            // Rollback, close everything, possibly compensate for any permanent changes  
            // during the conversation, and finally restart business conversation. Maybe  
            // give the user of the application a chance to merge some of his work with  
            // fresh data... what you do here depends on your applications design.  
            throw staleEx;  
        } catch (Throwable ex) {  
            // Rollback only  
            ex.printStackTrace();  
            try {  
                if (s.getTransaction().isActive()) {  
                    log.debug("Trying to rollback database transaction after exception");  
                    s.getTransaction().rollback();  
                }  
            } catch (Throwable rbEx) {  
                log.error("Could not rollback transaction after exception!", rbEx);  
            }  
  
            // Let others handle it... maybe another interceptor for exceptions?  
            throw new ServletException(ex);  
        }  
    }  
  
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {  
        log.debug("Initializing filter...");  
        log.debug("Obtaining SessionFactory from static HibernateUtil singleton");  
        sf = HibernateUtil.getSessionFactory();  
    }  
  
    @Override
    public void destroy() {}  
  
}  
