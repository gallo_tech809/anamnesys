/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.utileria.filtros;

import com.gallotech.gallotech.controlador.NavigationBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author avasquez
 */
@WebFilter(filterName = "FiltroDeAcceso", urlPatterns = {"/faces/app/*"}, dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class FiltroDeAcceso implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    private static String urlRequest = "";

    public FiltroDeAcceso() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest hsreq = (HttpServletRequest) request;
        HttpServletResponse hsres = (HttpServletResponse) response;
        Object userName = hsreq.getSession().getAttribute("username");
        
        // 14/08/2014 Obtener el código sistema del archivo de propiedades codigoSistema.properties
        
        Short codigoSistema = 1;
        System.out.println("CODIGO SISTEMA DESDE FILTRO : " + codigoSistema);
        
  
        
        Properties p = new Properties();
            p.load(getClass().getResourceAsStream("/com/gallotech/gallotech/web/config/codigoSistemas.properties"));
           Object objetoCodigoSistema = p.getProperty("codigoSistema");
           
           System.out.println("codigo: "+objetoCodigoSistema.toString());
        
        
    
        try {
            if (userName == null) {
                urlRequest = hsreq.getRequestURL().toString();
                String rutaIndex = new NavigationBean(Integer.valueOf(objetoCodigoSistema.toString())).getLogoutURL();
                System.out.println("EL USUARIO ES NULO Y SE ASIGNARA RUTA: "+rutaIndex);
               // hsres.sendRedirect("/Newsoft_Caja_Chica");
                hsres.sendRedirect(rutaIndex);
               
            } else {
                chain.doFilter(request, response);
            }
        } catch (IOException | ServletException t) {
            System.out.println(t.getMessage());
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("FiltroDeAcceso()");
        }
        StringBuilder sb = new StringBuilder("FiltroDeAcceso(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
}
