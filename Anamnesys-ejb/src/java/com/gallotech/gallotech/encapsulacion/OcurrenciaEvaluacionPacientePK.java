/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alvasrey
 */
@Embeddable
public class OcurrenciaEvaluacionPacientePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Evaluacion")
    private BigInteger codigoEvaluacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Paciente")
    private BigInteger codigoPaciente;

    public OcurrenciaEvaluacionPacientePK() {
    }

    public OcurrenciaEvaluacionPacientePK(BigInteger codigoEvaluacion, BigInteger codigoPaciente) {
        this.codigoEvaluacion = codigoEvaluacion;
        this.codigoPaciente = codigoPaciente;
    }

    public BigInteger getCodigoEvaluacion() {
        return codigoEvaluacion;
    }

    public void setCodigoEvaluacion(BigInteger codigoEvaluacion) {
        this.codigoEvaluacion = codigoEvaluacion;
    }

    public BigInteger getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(BigInteger codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEvaluacion != null ? codigoEvaluacion.hashCode() : 0);
        hash += (codigoPaciente != null ? codigoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OcurrenciaEvaluacionPacientePK)) {
            return false;
        }
        OcurrenciaEvaluacionPacientePK other = (OcurrenciaEvaluacionPacientePK) object;
        if ((this.codigoEvaluacion == null && other.codigoEvaluacion != null) || (this.codigoEvaluacion != null && !this.codigoEvaluacion.equals(other.codigoEvaluacion))) {
            return false;
        }
        if ((this.codigoPaciente == null && other.codigoPaciente != null) || (this.codigoPaciente != null && !this.codigoPaciente.equals(other.codigoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.OcurrenciaEvaluacionPacientePK[ codigoEvaluacion=" + codigoEvaluacion + ", codigoPaciente=" + codigoPaciente + " ]";
    }
    
}
