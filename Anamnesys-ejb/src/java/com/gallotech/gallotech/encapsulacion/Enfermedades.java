/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Enfermedades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enfermedades.findAll", query = "SELECT e FROM Enfermedades e"),
    @NamedQuery(name = "Enfermedades.findByCodigoEnfermedad", query = "SELECT e FROM Enfermedades e WHERE e.codigoEnfermedad = :codigoEnfermedad"),
    @NamedQuery(name = "Enfermedades.findByDescripcionEnfermedad", query = "SELECT e FROM Enfermedades e WHERE e.descripcionEnfermedad = :descripcionEnfermedad"),
    @NamedQuery(name = "Enfermedades.findBySintomasEnfermedad", query = "SELECT e FROM Enfermedades e WHERE e.sintomasEnfermedad = :sintomasEnfermedad")})
public class Enfermedades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Enfermedad")
    private Long codigoEnfermedad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Enfermedad")
    private String descripcionEnfermedad;
    @Size(max = 2048)
    @Column(name = "Sintomas_Enfermedad")
    private String sintomasEnfermedad;
    @OneToMany(mappedBy = "codigoEnfermedad", fetch = FetchType.LAZY)
    private List<EnfermedadesPacientes> enfermedadesPacientesList;

    public Enfermedades() {
    }

    public Enfermedades(Long codigoEnfermedad) {
        this.codigoEnfermedad = codigoEnfermedad;
    }

    public Enfermedades(Long codigoEnfermedad, String descripcionEnfermedad) {
        this.codigoEnfermedad = codigoEnfermedad;
        this.descripcionEnfermedad = descripcionEnfermedad;
    }

    public Long getCodigoEnfermedad() {
        return codigoEnfermedad;
    }

    public void setCodigoEnfermedad(Long codigoEnfermedad) {
        this.codigoEnfermedad = codigoEnfermedad;
    }

    public String getDescripcionEnfermedad() {
        return descripcionEnfermedad;
    }

    public void setDescripcionEnfermedad(String descripcionEnfermedad) {
        this.descripcionEnfermedad = descripcionEnfermedad;
    }

    public String getSintomasEnfermedad() {
        return sintomasEnfermedad;
    }

    public void setSintomasEnfermedad(String sintomasEnfermedad) {
        this.sintomasEnfermedad = sintomasEnfermedad;
    }

    @XmlTransient
    public List<EnfermedadesPacientes> getEnfermedadesPacientesList() {
        return enfermedadesPacientesList;
    }

    public void setEnfermedadesPacientesList(List<EnfermedadesPacientes> enfermedadesPacientesList) {
        this.enfermedadesPacientesList = enfermedadesPacientesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEnfermedad != null ? codigoEnfermedad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enfermedades)) {
            return false;
        }
        Enfermedades other = (Enfermedades) object;
        if ((this.codigoEnfermedad == null && other.codigoEnfermedad != null) || (this.codigoEnfermedad != null && !this.codigoEnfermedad.equals(other.codigoEnfermedad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Enfermedades[ codigoEnfermedad=" + codigoEnfermedad + " ]";
    }
    
}
