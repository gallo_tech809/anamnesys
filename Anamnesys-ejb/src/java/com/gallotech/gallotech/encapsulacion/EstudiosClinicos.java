/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Estudios_Clinicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstudiosClinicos.findAll", query = "SELECT e FROM EstudiosClinicos e"),
    @NamedQuery(name = "EstudiosClinicos.findByCodigoEstudioClinico", query = "SELECT e FROM EstudiosClinicos e WHERE e.codigoEstudioClinico = :codigoEstudioClinico"),
    @NamedQuery(name = "EstudiosClinicos.findByDescripcionEstudioClinico", query = "SELECT e FROM EstudiosClinicos e WHERE e.descripcionEstudioClinico = :descripcionEstudioClinico")})
public class EstudiosClinicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Estudio_Clinico")
    private Long codigoEstudioClinico;
    @Size(max = 128)
    @Column(name = "Descripcion_Estudio_Clinico")
    private String descripcionEstudioClinico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoEstudioClinico", fetch = FetchType.LAZY)
    private List<EstudioClinicoPaciente> estudioClinicoPacienteList;

    public EstudiosClinicos() {
    }

    public EstudiosClinicos(Long codigoEstudioClinico) {
        this.codigoEstudioClinico = codigoEstudioClinico;
    }

    public Long getCodigoEstudioClinico() {
        return codigoEstudioClinico;
    }

    public void setCodigoEstudioClinico(Long codigoEstudioClinico) {
        this.codigoEstudioClinico = codigoEstudioClinico;
    }

    public String getDescripcionEstudioClinico() {
        return descripcionEstudioClinico;
    }

    public void setDescripcionEstudioClinico(String descripcionEstudioClinico) {
        this.descripcionEstudioClinico = descripcionEstudioClinico;
    }

    @XmlTransient
    public List<EstudioClinicoPaciente> getEstudioClinicoPacienteList() {
        return estudioClinicoPacienteList;
    }

    public void setEstudioClinicoPacienteList(List<EstudioClinicoPaciente> estudioClinicoPacienteList) {
        this.estudioClinicoPacienteList = estudioClinicoPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEstudioClinico != null ? codigoEstudioClinico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudiosClinicos)) {
            return false;
        }
        EstudiosClinicos other = (EstudiosClinicos) object;
        if ((this.codigoEstudioClinico == null && other.codigoEstudioClinico != null) || (this.codigoEstudioClinico != null && !this.codigoEstudioClinico.equals(other.codigoEstudioClinico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.EstudiosClinicos[ codigoEstudioClinico=" + codigoEstudioClinico + " ]";
    }
    
}
