/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Accesos_Usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccesosUsuarios.findAll", query = "SELECT a FROM AccesosUsuarios a"),
    @NamedQuery(name = "AccesosUsuarios.findByCodigoUsuario", query = "SELECT a FROM AccesosUsuarios a WHERE a.accesosUsuariosPK.codigoUsuario = :codigoUsuario"),
    @NamedQuery(name = "AccesosUsuarios.findByCodigoNivel", query = "SELECT a FROM AccesosUsuarios a WHERE a.accesosUsuariosPK.codigoNivel = :codigoNivel"),
    @NamedQuery(name = "AccesosUsuarios.findByOpcionMenu", query = "SELECT a FROM AccesosUsuarios a WHERE a.opcionMenu = :opcionMenu")})
public class AccesosUsuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccesosUsuariosPK accesosUsuariosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "Opcion_Menu")
    private String opcionMenu;
    @JoinColumn(name = "Codigo_Nivel", referencedColumnName = "Codigo_Nivel", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private NivelesAccesos nivelesAccesos;
    @JoinColumn(name = "Codigo_Usuario", referencedColumnName = "Codigo_Usuario", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Usuarios usuarios;

    public AccesosUsuarios() {
    }

    public AccesosUsuarios(AccesosUsuariosPK accesosUsuariosPK) {
        this.accesosUsuariosPK = accesosUsuariosPK;
    }

    public AccesosUsuarios(AccesosUsuariosPK accesosUsuariosPK, String opcionMenu) {
        this.accesosUsuariosPK = accesosUsuariosPK;
        this.opcionMenu = opcionMenu;
    }

    public AccesosUsuarios(long codigoUsuario, long codigoNivel) {
        this.accesosUsuariosPK = new AccesosUsuariosPK(codigoUsuario, codigoNivel);
    }

    public AccesosUsuariosPK getAccesosUsuariosPK() {
        return accesosUsuariosPK;
    }

    public void setAccesosUsuariosPK(AccesosUsuariosPK accesosUsuariosPK) {
        this.accesosUsuariosPK = accesosUsuariosPK;
    }

    public String getOpcionMenu() {
        return opcionMenu;
    }

    public void setOpcionMenu(String opcionMenu) {
        this.opcionMenu = opcionMenu;
    }

    public NivelesAccesos getNivelesAccesos() {
        return nivelesAccesos;
    }

    public void setNivelesAccesos(NivelesAccesos nivelesAccesos) {
        this.nivelesAccesos = nivelesAccesos;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accesosUsuariosPK != null ? accesosUsuariosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccesosUsuarios)) {
            return false;
        }
        AccesosUsuarios other = (AccesosUsuarios) object;
        if ((this.accesosUsuariosPK == null && other.accesosUsuariosPK != null) || (this.accesosUsuariosPK != null && !this.accesosUsuariosPK.equals(other.accesosUsuariosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.AccesosUsuarios[ accesosUsuariosPK=" + accesosUsuariosPK + " ]";
    }
    
}
