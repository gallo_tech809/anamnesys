/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Tipos_Medio_Comunicacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposMedioComunicacion.findAll", query = "SELECT t FROM TiposMedioComunicacion t"),
    @NamedQuery(name = "TiposMedioComunicacion.findByCodigoTipoMedioComunicacion", query = "SELECT t FROM TiposMedioComunicacion t WHERE t.codigoTipoMedioComunicacion = :codigoTipoMedioComunicacion"),
    @NamedQuery(name = "TiposMedioComunicacion.findByDescripcionTipoMedioComunicacion", query = "SELECT t FROM TiposMedioComunicacion t WHERE t.descripcionTipoMedioComunicacion = :descripcionTipoMedioComunicacion")})
public class TiposMedioComunicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Tipo_Medio_Comunicacion")
    private Integer codigoTipoMedioComunicacion;
    @Size(max = 40)
    @Column(name = "Descripcion_Tipo_Medio_Comunicacion")
    private String descripcionTipoMedioComunicacion;
    @OneToMany(mappedBy = "codigoTipoMedioComunicacion", fetch = FetchType.LAZY)
    private List<MedioComunicacionFamiliar> medioComunicacionFamiliarList;
    @OneToMany(mappedBy = "codigoTipoMedioComunicacion", fetch = FetchType.LAZY)
    private List<MedioComunicacionLaboratorio> medioComunicacionLaboratorioList;
    @OneToMany(mappedBy = "codigoTipoMedioComunicacion", fetch = FetchType.LAZY)
    private List<MedioComunicacionPaciente> medioComunicacionPacienteList;

    public TiposMedioComunicacion() {
    }

    public TiposMedioComunicacion(Integer codigoTipoMedioComunicacion) {
        this.codigoTipoMedioComunicacion = codigoTipoMedioComunicacion;
    }

    public Integer getCodigoTipoMedioComunicacion() {
        return codigoTipoMedioComunicacion;
    }

    public void setCodigoTipoMedioComunicacion(Integer codigoTipoMedioComunicacion) {
        this.codigoTipoMedioComunicacion = codigoTipoMedioComunicacion;
    }

    public String getDescripcionTipoMedioComunicacion() {
        return descripcionTipoMedioComunicacion;
    }

    public void setDescripcionTipoMedioComunicacion(String descripcionTipoMedioComunicacion) {
        this.descripcionTipoMedioComunicacion = descripcionTipoMedioComunicacion;
    }

    @XmlTransient
    public List<MedioComunicacionFamiliar> getMedioComunicacionFamiliarList() {
        return medioComunicacionFamiliarList;
    }

    public void setMedioComunicacionFamiliarList(List<MedioComunicacionFamiliar> medioComunicacionFamiliarList) {
        this.medioComunicacionFamiliarList = medioComunicacionFamiliarList;
    }

    @XmlTransient
    public List<MedioComunicacionLaboratorio> getMedioComunicacionLaboratorioList() {
        return medioComunicacionLaboratorioList;
    }

    public void setMedioComunicacionLaboratorioList(List<MedioComunicacionLaboratorio> medioComunicacionLaboratorioList) {
        this.medioComunicacionLaboratorioList = medioComunicacionLaboratorioList;
    }

    @XmlTransient
    public List<MedioComunicacionPaciente> getMedioComunicacionPacienteList() {
        return medioComunicacionPacienteList;
    }

    public void setMedioComunicacionPacienteList(List<MedioComunicacionPaciente> medioComunicacionPacienteList) {
        this.medioComunicacionPacienteList = medioComunicacionPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoTipoMedioComunicacion != null ? codigoTipoMedioComunicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposMedioComunicacion)) {
            return false;
        }
        TiposMedioComunicacion other = (TiposMedioComunicacion) object;
        if ((this.codigoTipoMedioComunicacion == null && other.codigoTipoMedioComunicacion != null) || (this.codigoTipoMedioComunicacion != null && !this.codigoTipoMedioComunicacion.equals(other.codigoTipoMedioComunicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.TiposMedioComunicacion[ codigoTipoMedioComunicacion=" + codigoTipoMedioComunicacion + " ]";
    }
    
}
