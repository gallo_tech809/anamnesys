/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Medio_Comunicacion_Laboratorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedioComunicacionLaboratorio.findAll", query = "SELECT m FROM MedioComunicacionLaboratorio m"),
    @NamedQuery(name = "MedioComunicacionLaboratorio.findByCodigoMedioComunicacionLaboratorio", query = "SELECT m FROM MedioComunicacionLaboratorio m WHERE m.codigoMedioComunicacionLaboratorio = :codigoMedioComunicacionLaboratorio"),
    @NamedQuery(name = "MedioComunicacionLaboratorio.findByDescripcion", query = "SELECT m FROM MedioComunicacionLaboratorio m WHERE m.descripcion = :descripcion")})
public class MedioComunicacionLaboratorio implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Medio_Comunicacion_Laboratorio")
    private BigDecimal codigoMedioComunicacionLaboratorio;
    @Size(max = 20)
    @Column(name = "Descripcion")
    private String descripcion;
    @JoinColumn(name = "Codigo_Laboratorio", referencedColumnName = "Codigo_Laboratorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private Laboratorio codigoLaboratorio;
    @JoinColumn(name = "Codigo_Tipo_Medio_Comunicacion", referencedColumnName = "Codigo_Tipo_Medio_Comunicacion")
    @ManyToOne(fetch = FetchType.LAZY)
    private TiposMedioComunicacion codigoTipoMedioComunicacion;

    public MedioComunicacionLaboratorio() {
    }

    public MedioComunicacionLaboratorio(BigDecimal codigoMedioComunicacionLaboratorio) {
        this.codigoMedioComunicacionLaboratorio = codigoMedioComunicacionLaboratorio;
    }

    public BigDecimal getCodigoMedioComunicacionLaboratorio() {
        return codigoMedioComunicacionLaboratorio;
    }

    public void setCodigoMedioComunicacionLaboratorio(BigDecimal codigoMedioComunicacionLaboratorio) {
        this.codigoMedioComunicacionLaboratorio = codigoMedioComunicacionLaboratorio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Laboratorio getCodigoLaboratorio() {
        return codigoLaboratorio;
    }

    public void setCodigoLaboratorio(Laboratorio codigoLaboratorio) {
        this.codigoLaboratorio = codigoLaboratorio;
    }

    public TiposMedioComunicacion getCodigoTipoMedioComunicacion() {
        return codigoTipoMedioComunicacion;
    }

    public void setCodigoTipoMedioComunicacion(TiposMedioComunicacion codigoTipoMedioComunicacion) {
        this.codigoTipoMedioComunicacion = codigoTipoMedioComunicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoMedioComunicacionLaboratorio != null ? codigoMedioComunicacionLaboratorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioComunicacionLaboratorio)) {
            return false;
        }
        MedioComunicacionLaboratorio other = (MedioComunicacionLaboratorio) object;
        if ((this.codigoMedioComunicacionLaboratorio == null && other.codigoMedioComunicacionLaboratorio != null) || (this.codigoMedioComunicacionLaboratorio != null && !this.codigoMedioComunicacionLaboratorio.equals(other.codigoMedioComunicacionLaboratorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.MedioComunicacionLaboratorio[ codigoMedioComunicacionLaboratorio=" + codigoMedioComunicacionLaboratorio + " ]";
    }
    
}
