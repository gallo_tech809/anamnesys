/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Consultorio_Doctor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConsultorioDoctor.findAll", query = "SELECT c FROM ConsultorioDoctor c"),
    @NamedQuery(name = "ConsultorioDoctor.findByCodigoDoctor", query = "SELECT c FROM ConsultorioDoctor c WHERE c.consultorioDoctorPK.codigoDoctor = :codigoDoctor"),
    @NamedQuery(name = "ConsultorioDoctor.findByCodigoConsultorio", query = "SELECT c FROM ConsultorioDoctor c WHERE c.consultorioDoctorPK.codigoConsultorio = :codigoConsultorio"),
    @NamedQuery(name = "ConsultorioDoctor.findByStatus", query = "SELECT c FROM ConsultorioDoctor c WHERE c.status = :status")})
public class ConsultorioDoctor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConsultorioDoctorPK consultorioDoctorPK;
    @Size(max = 15)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "Codigo_Consultorio", referencedColumnName = "Codigo_Consultorio", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Consultorios consultorios;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Doctores doctores;

    public ConsultorioDoctor() {
    }

    public ConsultorioDoctor(ConsultorioDoctorPK consultorioDoctorPK) {
        this.consultorioDoctorPK = consultorioDoctorPK;
    }

    public ConsultorioDoctor(long codigoDoctor, long codigoConsultorio) {
        this.consultorioDoctorPK = new ConsultorioDoctorPK(codigoDoctor, codigoConsultorio);
    }

    public ConsultorioDoctorPK getConsultorioDoctorPK() {
        return consultorioDoctorPK;
    }

    public void setConsultorioDoctorPK(ConsultorioDoctorPK consultorioDoctorPK) {
        this.consultorioDoctorPK = consultorioDoctorPK;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Consultorios getConsultorios() {
        return consultorios;
    }

    public void setConsultorios(Consultorios consultorios) {
        this.consultorios = consultorios;
    }

    public Doctores getDoctores() {
        return doctores;
    }

    public void setDoctores(Doctores doctores) {
        this.doctores = doctores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consultorioDoctorPK != null ? consultorioDoctorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultorioDoctor)) {
            return false;
        }
        ConsultorioDoctor other = (ConsultorioDoctor) object;
        if ((this.consultorioDoctorPK == null && other.consultorioDoctorPK != null) || (this.consultorioDoctorPK != null && !this.consultorioDoctorPK.equals(other.consultorioDoctorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ConsultorioDoctor[ consultorioDoctorPK=" + consultorioDoctorPK + " ]";
    }
    
}
