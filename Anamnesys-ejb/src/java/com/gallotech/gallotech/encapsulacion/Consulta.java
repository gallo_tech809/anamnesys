/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Consulta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c"),
    @NamedQuery(name = "Consulta.findByCodigoConsulta", query = "SELECT c FROM Consulta c WHERE c.codigoConsulta = :codigoConsulta"),
    @NamedQuery(name = "Consulta.findByFechaConsulta", query = "SELECT c FROM Consulta c WHERE c.fechaConsulta = :fechaConsulta"),
    @NamedQuery(name = "Consulta.findByMotivoConsulta", query = "SELECT c FROM Consulta c WHERE c.motivoConsulta = :motivoConsulta"),
    @NamedQuery(name = "Consulta.findByHistoriaActual", query = "SELECT c FROM Consulta c WHERE c.historiaActual = :historiaActual")})
public class Consulta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Consulta")
    private Long codigoConsulta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Consulta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaConsulta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2048)
    @Column(name = "Motivo_Consulta")
    private String motivoConsulta;
    @Size(max = 2048)
    @Column(name = "Historia_Actual")
    private String historiaActual;
    @OneToMany(mappedBy = "codigoConsulta", fetch = FetchType.LAZY)
    private List<EnfermedadesPacientes> enfermedadesPacientesList;
    @JoinColumn(name = "Codigo_ARS", referencedColumnName = "Codigo_ARS")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ars codigoARS;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;
    @OneToMany(mappedBy = "codigoConsulta", fetch = FetchType.LAZY)
    private List<RecetaPaciente> recetaPacienteList;

    public Consulta() {
    }

    public Consulta(Long codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

    public Consulta(Long codigoConsulta, Date fechaConsulta, String motivoConsulta) {
        this.codigoConsulta = codigoConsulta;
        this.fechaConsulta = fechaConsulta;
        this.motivoConsulta = motivoConsulta;
    }

    public Long getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(Long codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

    public Date getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(Date fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public String getHistoriaActual() {
        return historiaActual;
    }

    public void setHistoriaActual(String historiaActual) {
        this.historiaActual = historiaActual;
    }

    @XmlTransient
    public List<EnfermedadesPacientes> getEnfermedadesPacientesList() {
        return enfermedadesPacientesList;
    }

    public void setEnfermedadesPacientesList(List<EnfermedadesPacientes> enfermedadesPacientesList) {
        this.enfermedadesPacientesList = enfermedadesPacientesList;
    }

    public Ars getCodigoARS() {
        return codigoARS;
    }

    public void setCodigoARS(Ars codigoARS) {
        this.codigoARS = codigoARS;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @XmlTransient
    public List<RecetaPaciente> getRecetaPacienteList() {
        return recetaPacienteList;
    }

    public void setRecetaPacienteList(List<RecetaPaciente> recetaPacienteList) {
        this.recetaPacienteList = recetaPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoConsulta != null ? codigoConsulta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.codigoConsulta == null && other.codigoConsulta != null) || (this.codigoConsulta != null && !this.codigoConsulta.equals(other.codigoConsulta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Consulta[ codigoConsulta=" + codigoConsulta + " ]";
    }
    
}
