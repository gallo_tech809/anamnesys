/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Medio_Comunicacion_Familiar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedioComunicacionFamiliar.findAll", query = "SELECT m FROM MedioComunicacionFamiliar m"),
    @NamedQuery(name = "MedioComunicacionFamiliar.findByCodigoMedioComunicacionFamiliarPaciente", query = "SELECT m FROM MedioComunicacionFamiliar m WHERE m.codigoMedioComunicacionFamiliarPaciente = :codigoMedioComunicacionFamiliarPaciente"),
    @NamedQuery(name = "MedioComunicacionFamiliar.findByDescripcion", query = "SELECT m FROM MedioComunicacionFamiliar m WHERE m.descripcion = :descripcion")})
public class MedioComunicacionFamiliar implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Medio_Comunicacion_Familiar_Paciente")
    private BigDecimal codigoMedioComunicacionFamiliarPaciente;
    @Size(max = 20)
    @Column(name = "Descripcion")
    private String descripcion;
    @JoinColumn(name = "Codigo_Familiar_Paciente", referencedColumnName = "Codigo_Familiar_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private FamiliaresPacientes codigoFamiliarPaciente;
    @JoinColumn(name = "Codigo_Tipo_Medio_Comunicacion", referencedColumnName = "Codigo_Tipo_Medio_Comunicacion")
    @ManyToOne(fetch = FetchType.LAZY)
    private TiposMedioComunicacion codigoTipoMedioComunicacion;

    public MedioComunicacionFamiliar() {
    }

    public MedioComunicacionFamiliar(BigDecimal codigoMedioComunicacionFamiliarPaciente) {
        this.codigoMedioComunicacionFamiliarPaciente = codigoMedioComunicacionFamiliarPaciente;
    }

    public BigDecimal getCodigoMedioComunicacionFamiliarPaciente() {
        return codigoMedioComunicacionFamiliarPaciente;
    }

    public void setCodigoMedioComunicacionFamiliarPaciente(BigDecimal codigoMedioComunicacionFamiliarPaciente) {
        this.codigoMedioComunicacionFamiliarPaciente = codigoMedioComunicacionFamiliarPaciente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FamiliaresPacientes getCodigoFamiliarPaciente() {
        return codigoFamiliarPaciente;
    }

    public void setCodigoFamiliarPaciente(FamiliaresPacientes codigoFamiliarPaciente) {
        this.codigoFamiliarPaciente = codigoFamiliarPaciente;
    }

    public TiposMedioComunicacion getCodigoTipoMedioComunicacion() {
        return codigoTipoMedioComunicacion;
    }

    public void setCodigoTipoMedioComunicacion(TiposMedioComunicacion codigoTipoMedioComunicacion) {
        this.codigoTipoMedioComunicacion = codigoTipoMedioComunicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoMedioComunicacionFamiliarPaciente != null ? codigoMedioComunicacionFamiliarPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioComunicacionFamiliar)) {
            return false;
        }
        MedioComunicacionFamiliar other = (MedioComunicacionFamiliar) object;
        if ((this.codigoMedioComunicacionFamiliarPaciente == null && other.codigoMedioComunicacionFamiliarPaciente != null) || (this.codigoMedioComunicacionFamiliarPaciente != null && !this.codigoMedioComunicacionFamiliarPaciente.equals(other.codigoMedioComunicacionFamiliarPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.MedioComunicacionFamiliar[ codigoMedioComunicacionFamiliarPaciente=" + codigoMedioComunicacionFamiliarPaciente + " ]";
    }
    
}
