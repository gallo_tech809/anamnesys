/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Evaluaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evaluaciones.findAll", query = "SELECT e FROM Evaluaciones e"),
    @NamedQuery(name = "Evaluaciones.findByCodigoEvaluacion", query = "SELECT e FROM Evaluaciones e WHERE e.codigoEvaluacion = :codigoEvaluacion"),
    @NamedQuery(name = "Evaluaciones.findByDescripcionEvaluacion", query = "SELECT e FROM Evaluaciones e WHERE e.descripcionEvaluacion = :descripcionEvaluacion"),
    @NamedQuery(name = "Evaluaciones.findByTipoDatoPresentar", query = "SELECT e FROM Evaluaciones e WHERE e.tipoDatoPresentar = :tipoDatoPresentar")})
public class Evaluaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Evaluacion")
    private BigDecimal codigoEvaluacion;
    @Size(max = 40)
    @Column(name = "Descripcion_Evaluacion")
    private String descripcionEvaluacion;
    @Size(max = 20)
    @Column(name = "Tipo_Dato_Presentar")
    private String tipoDatoPresentar;
    @JoinColumn(name = "Codigo_Especialidad", referencedColumnName = "Codigo_Especialidad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Especialidades codigoEspecialidad;
    @JoinColumn(name = "Codigo_Tipo_Evaluacion", referencedColumnName = "Codigo_Tipo_Evaluacion")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoEvaluacion codigoTipoEvaluacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluaciones", fetch = FetchType.LAZY)
    private List<OcurrenciaEvaluacionPaciente> ocurrenciaEvaluacionPacienteList;

    public Evaluaciones() {
    }

    public Evaluaciones(BigDecimal codigoEvaluacion) {
        this.codigoEvaluacion = codigoEvaluacion;
    }

    public BigDecimal getCodigoEvaluacion() {
        return codigoEvaluacion;
    }

    public void setCodigoEvaluacion(BigDecimal codigoEvaluacion) {
        this.codigoEvaluacion = codigoEvaluacion;
    }

    public String getDescripcionEvaluacion() {
        return descripcionEvaluacion;
    }

    public void setDescripcionEvaluacion(String descripcionEvaluacion) {
        this.descripcionEvaluacion = descripcionEvaluacion;
    }

    public String getTipoDatoPresentar() {
        return tipoDatoPresentar;
    }

    public void setTipoDatoPresentar(String tipoDatoPresentar) {
        this.tipoDatoPresentar = tipoDatoPresentar;
    }

    public Especialidades getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(Especialidades codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public TipoEvaluacion getCodigoTipoEvaluacion() {
        return codigoTipoEvaluacion;
    }

    public void setCodigoTipoEvaluacion(TipoEvaluacion codigoTipoEvaluacion) {
        this.codigoTipoEvaluacion = codigoTipoEvaluacion;
    }

    @XmlTransient
    public List<OcurrenciaEvaluacionPaciente> getOcurrenciaEvaluacionPacienteList() {
        return ocurrenciaEvaluacionPacienteList;
    }

    public void setOcurrenciaEvaluacionPacienteList(List<OcurrenciaEvaluacionPaciente> ocurrenciaEvaluacionPacienteList) {
        this.ocurrenciaEvaluacionPacienteList = ocurrenciaEvaluacionPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEvaluacion != null ? codigoEvaluacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evaluaciones)) {
            return false;
        }
        Evaluaciones other = (Evaluaciones) object;
        if ((this.codigoEvaluacion == null && other.codigoEvaluacion != null) || (this.codigoEvaluacion != null && !this.codigoEvaluacion.equals(other.codigoEvaluacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Evaluaciones[ codigoEvaluacion=" + codigoEvaluacion + " ]";
    }
    
}
