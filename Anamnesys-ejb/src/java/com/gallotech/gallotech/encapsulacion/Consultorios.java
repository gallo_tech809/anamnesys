/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Consultorios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultorios.findAll", query = "SELECT c FROM Consultorios c"),
    @NamedQuery(name = "Consultorios.findByCodigoConsultorio", query = "SELECT c FROM Consultorios c WHERE c.codigoConsultorio = :codigoConsultorio"),
    @NamedQuery(name = "Consultorios.findByDescripcionConsultorio", query = "SELECT c FROM Consultorios c WHERE c.descripcionConsultorio = :descripcionConsultorio"),
    @NamedQuery(name = "Consultorios.findByPisoConsultorio", query = "SELECT c FROM Consultorios c WHERE c.pisoConsultorio = :pisoConsultorio"),
    @NamedQuery(name = "Consultorios.findByNumeroConsultorio", query = "SELECT c FROM Consultorios c WHERE c.numeroConsultorio = :numeroConsultorio")})
public class Consultorios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Consultorio")
    private Long codigoConsultorio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Consultorio")
    private String descripcionConsultorio;
    @Size(max = 20)
    @Column(name = "Piso_Consultorio")
    private String pisoConsultorio;
    @Size(max = 20)
    @Column(name = "Numero_Consultorio")
    private String numeroConsultorio;
    @OneToMany(mappedBy = "codigoConsultorio", fetch = FetchType.LAZY)
    private List<CitasPaciente> citasPacienteList;
    @OneToMany(mappedBy = "codigoConsultorio", fetch = FetchType.LAZY)
    private List<MedioComunicacionConsultorio> medioComunicacionConsultorioList;
    @OneToMany(mappedBy = "codigoConsultorio", fetch = FetchType.LAZY)
    private List<EncabezadoFactura> encabezadoFacturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "consultorios", fetch = FetchType.LAZY)
    private List<ConsultorioDoctor> consultorioDoctorList;
    @JoinColumn(name = "Codigo_Hospital", referencedColumnName = "Codigo_Hospital")
    @ManyToOne(fetch = FetchType.LAZY)
    private Hospitales codigoHospital;

    public Consultorios() {
    }

    public Consultorios(Long codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    public Consultorios(Long codigoConsultorio, String descripcionConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
        this.descripcionConsultorio = descripcionConsultorio;
    }

    public Long getCodigoConsultorio() {
        return codigoConsultorio;
    }

    public void setCodigoConsultorio(Long codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    public String getDescripcionConsultorio() {
        return descripcionConsultorio;
    }

    public void setDescripcionConsultorio(String descripcionConsultorio) {
        this.descripcionConsultorio = descripcionConsultorio;
    }

    public String getPisoConsultorio() {
        return pisoConsultorio;
    }

    public void setPisoConsultorio(String pisoConsultorio) {
        this.pisoConsultorio = pisoConsultorio;
    }

    public String getNumeroConsultorio() {
        return numeroConsultorio;
    }

    public void setNumeroConsultorio(String numeroConsultorio) {
        this.numeroConsultorio = numeroConsultorio;
    }

    @XmlTransient
    public List<CitasPaciente> getCitasPacienteList() {
        return citasPacienteList;
    }

    public void setCitasPacienteList(List<CitasPaciente> citasPacienteList) {
        this.citasPacienteList = citasPacienteList;
    }

    @XmlTransient
    public List<MedioComunicacionConsultorio> getMedioComunicacionConsultorioList() {
        return medioComunicacionConsultorioList;
    }

    public void setMedioComunicacionConsultorioList(List<MedioComunicacionConsultorio> medioComunicacionConsultorioList) {
        this.medioComunicacionConsultorioList = medioComunicacionConsultorioList;
    }

    @XmlTransient
    public List<EncabezadoFactura> getEncabezadoFacturaList() {
        return encabezadoFacturaList;
    }

    public void setEncabezadoFacturaList(List<EncabezadoFactura> encabezadoFacturaList) {
        this.encabezadoFacturaList = encabezadoFacturaList;
    }

    @XmlTransient
    public List<ConsultorioDoctor> getConsultorioDoctorList() {
        return consultorioDoctorList;
    }

    public void setConsultorioDoctorList(List<ConsultorioDoctor> consultorioDoctorList) {
        this.consultorioDoctorList = consultorioDoctorList;
    }

    public Hospitales getCodigoHospital() {
        return codigoHospital;
    }

    public void setCodigoHospital(Hospitales codigoHospital) {
        this.codigoHospital = codigoHospital;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoConsultorio != null ? codigoConsultorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultorios)) {
            return false;
        }
        Consultorios other = (Consultorios) object;
        if ((this.codigoConsultorio == null && other.codigoConsultorio != null) || (this.codigoConsultorio != null && !this.codigoConsultorio.equals(other.codigoConsultorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Consultorios[ codigoConsultorio=" + codigoConsultorio + " ]";
    }
    
}
