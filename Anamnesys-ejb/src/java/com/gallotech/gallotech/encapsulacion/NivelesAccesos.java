/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Niveles_Accesos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivelesAccesos.findAll", query = "SELECT n FROM NivelesAccesos n"),
    @NamedQuery(name = "NivelesAccesos.findByCodigoNivel", query = "SELECT n FROM NivelesAccesos n WHERE n.codigoNivel = :codigoNivel"),
    @NamedQuery(name = "NivelesAccesos.findByDescripcionNivel", query = "SELECT n FROM NivelesAccesos n WHERE n.descripcionNivel = :descripcionNivel")})
public class NivelesAccesos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Nivel")
    private Long codigoNivel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Nivel")
    private String descripcionNivel;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivelesAccesos", fetch = FetchType.LAZY)
    private List<AccesosUsuarios> accesosUsuariosList;

    public NivelesAccesos() {
    }

    public NivelesAccesos(Long codigoNivel) {
        this.codigoNivel = codigoNivel;
    }

    public NivelesAccesos(Long codigoNivel, String descripcionNivel) {
        this.codigoNivel = codigoNivel;
        this.descripcionNivel = descripcionNivel;
    }

    public Long getCodigoNivel() {
        return codigoNivel;
    }

    public void setCodigoNivel(Long codigoNivel) {
        this.codigoNivel = codigoNivel;
    }

    public String getDescripcionNivel() {
        return descripcionNivel;
    }

    public void setDescripcionNivel(String descripcionNivel) {
        this.descripcionNivel = descripcionNivel;
    }

    @XmlTransient
    public List<AccesosUsuarios> getAccesosUsuariosList() {
        return accesosUsuariosList;
    }

    public void setAccesosUsuariosList(List<AccesosUsuarios> accesosUsuariosList) {
        this.accesosUsuariosList = accesosUsuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoNivel != null ? codigoNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivelesAccesos)) {
            return false;
        }
        NivelesAccesos other = (NivelesAccesos) object;
        if ((this.codigoNivel == null && other.codigoNivel != null) || (this.codigoNivel != null && !this.codigoNivel.equals(other.codigoNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.NivelesAccesos[ codigoNivel=" + codigoNivel + " ]";
    }
    
}
