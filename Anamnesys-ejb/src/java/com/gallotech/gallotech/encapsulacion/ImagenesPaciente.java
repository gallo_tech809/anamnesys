/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Imagenes_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImagenesPaciente.findAll", query = "SELECT i FROM ImagenesPaciente i"),
    @NamedQuery(name = "ImagenesPaciente.findByCodigoImagenPaciente", query = "SELECT i FROM ImagenesPaciente i WHERE i.codigoImagenPaciente = :codigoImagenPaciente")})
public class ImagenesPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Imagen_Paciente")
    private BigDecimal codigoImagenPaciente;
    @Lob
    @Column(name = "Imagen")
    private Serializable imagen;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;

    public ImagenesPaciente() {
    }

    public ImagenesPaciente(BigDecimal codigoImagenPaciente) {
        this.codigoImagenPaciente = codigoImagenPaciente;
    }

    public BigDecimal getCodigoImagenPaciente() {
        return codigoImagenPaciente;
    }

    public void setCodigoImagenPaciente(BigDecimal codigoImagenPaciente) {
        this.codigoImagenPaciente = codigoImagenPaciente;
    }

    public Serializable getImagen() {
        return imagen;
    }

    public void setImagen(Serializable imagen) {
        this.imagen = imagen;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoImagenPaciente != null ? codigoImagenPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenesPaciente)) {
            return false;
        }
        ImagenesPaciente other = (ImagenesPaciente) object;
        if ((this.codigoImagenPaciente == null && other.codigoImagenPaciente != null) || (this.codigoImagenPaciente != null && !this.codigoImagenPaciente.equals(other.codigoImagenPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ImagenesPaciente[ codigoImagenPaciente=" + codigoImagenPaciente + " ]";
    }
    
}
