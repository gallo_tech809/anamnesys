/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Especialidades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Especialidades.findAll", query = "SELECT e FROM Especialidades e"),
    @NamedQuery(name = "Especialidades.findByCodigoEspecialidad", query = "SELECT e FROM Especialidades e WHERE e.codigoEspecialidad = :codigoEspecialidad"),
    @NamedQuery(name = "Especialidades.findByDescripcionEspecialidad", query = "SELECT e FROM Especialidades e WHERE e.descripcionEspecialidad = :descripcionEspecialidad")})
public class Especialidades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Especialidad")
    private Long codigoEspecialidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Especialidad")
    private String descripcionEspecialidad;
    @OneToMany(mappedBy = "codigoEspecialidad", fetch = FetchType.LAZY)
    private List<Doctores> doctoresList;
    @OneToMany(mappedBy = "codigoEspecialidad", fetch = FetchType.LAZY)
    private List<ProcedimientoMedico> procedimientoMedicoList;
    @OneToMany(mappedBy = "codigoEspecialidad", fetch = FetchType.LAZY)
    private List<ProcedimientoARS> procedimientoARSList;
    @OneToMany(mappedBy = "codigoEspecialidad", fetch = FetchType.LAZY)
    private List<Evaluaciones> evaluacionesList;

    public Especialidades() {
    }

    public Especialidades(Long codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public Especialidades(Long codigoEspecialidad, String descripcionEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
        this.descripcionEspecialidad = descripcionEspecialidad;
    }

    public Long getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(Long codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public String getDescripcionEspecialidad() {
        return descripcionEspecialidad;
    }

    public void setDescripcionEspecialidad(String descripcionEspecialidad) {
        this.descripcionEspecialidad = descripcionEspecialidad;
    }

    @XmlTransient
    public List<Doctores> getDoctoresList() {
        return doctoresList;
    }

    public void setDoctoresList(List<Doctores> doctoresList) {
        this.doctoresList = doctoresList;
    }

    @XmlTransient
    public List<ProcedimientoMedico> getProcedimientoMedicoList() {
        return procedimientoMedicoList;
    }

    public void setProcedimientoMedicoList(List<ProcedimientoMedico> procedimientoMedicoList) {
        this.procedimientoMedicoList = procedimientoMedicoList;
    }

    @XmlTransient
    public List<ProcedimientoARS> getProcedimientoARSList() {
        return procedimientoARSList;
    }

    public void setProcedimientoARSList(List<ProcedimientoARS> procedimientoARSList) {
        this.procedimientoARSList = procedimientoARSList;
    }

    @XmlTransient
    public List<Evaluaciones> getEvaluacionesList() {
        return evaluacionesList;
    }

    public void setEvaluacionesList(List<Evaluaciones> evaluacionesList) {
        this.evaluacionesList = evaluacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEspecialidad != null ? codigoEspecialidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Especialidades)) {
            return false;
        }
        Especialidades other = (Especialidades) object;
        if ((this.codigoEspecialidad == null && other.codigoEspecialidad != null) || (this.codigoEspecialidad != null && !this.codigoEspecialidad.equals(other.codigoEspecialidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Especialidades[ codigoEspecialidad=" + codigoEspecialidad + " ]";
    }
    
}
