/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Medio_Comunicacion_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedioComunicacionPaciente.findAll", query = "SELECT m FROM MedioComunicacionPaciente m"),
    @NamedQuery(name = "MedioComunicacionPaciente.findByCodigoMedioComunicacionPaciente", query = "SELECT m FROM MedioComunicacionPaciente m WHERE m.codigoMedioComunicacionPaciente = :codigoMedioComunicacionPaciente"),
    @NamedQuery(name = "MedioComunicacionPaciente.findByDescripcion", query = "SELECT m FROM MedioComunicacionPaciente m WHERE m.descripcion = :descripcion")})
public class MedioComunicacionPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Medio_Comunicacion_Paciente")
    private BigDecimal codigoMedioComunicacionPaciente;
    @Size(max = 20)
    @Column(name = "Descripcion")
    private String descripcion;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;
    @JoinColumn(name = "Codigo_Tipo_Medio_Comunicacion", referencedColumnName = "Codigo_Tipo_Medio_Comunicacion")
    @ManyToOne(fetch = FetchType.LAZY)
    private TiposMedioComunicacion codigoTipoMedioComunicacion;

    public MedioComunicacionPaciente() {
    }

    public MedioComunicacionPaciente(BigDecimal codigoMedioComunicacionPaciente) {
        this.codigoMedioComunicacionPaciente = codigoMedioComunicacionPaciente;
    }

    public BigDecimal getCodigoMedioComunicacionPaciente() {
        return codigoMedioComunicacionPaciente;
    }

    public void setCodigoMedioComunicacionPaciente(BigDecimal codigoMedioComunicacionPaciente) {
        this.codigoMedioComunicacionPaciente = codigoMedioComunicacionPaciente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    public TiposMedioComunicacion getCodigoTipoMedioComunicacion() {
        return codigoTipoMedioComunicacion;
    }

    public void setCodigoTipoMedioComunicacion(TiposMedioComunicacion codigoTipoMedioComunicacion) {
        this.codigoTipoMedioComunicacion = codigoTipoMedioComunicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoMedioComunicacionPaciente != null ? codigoMedioComunicacionPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioComunicacionPaciente)) {
            return false;
        }
        MedioComunicacionPaciente other = (MedioComunicacionPaciente) object;
        if ((this.codigoMedioComunicacionPaciente == null && other.codigoMedioComunicacionPaciente != null) || (this.codigoMedioComunicacionPaciente != null && !this.codigoMedioComunicacionPaciente.equals(other.codigoMedioComunicacionPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.MedioComunicacionPaciente[ codigoMedioComunicacionPaciente=" + codigoMedioComunicacionPaciente + " ]";
    }
    
}
