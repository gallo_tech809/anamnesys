/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Detalle_Factura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleFactura.findAll", query = "SELECT d FROM DetalleFactura d"),
    @NamedQuery(name = "DetalleFactura.findByAplicaPromocion", query = "SELECT d FROM DetalleFactura d WHERE d.aplicaPromocion = :aplicaPromocion"),
    @NamedQuery(name = "DetalleFactura.findByCodigoDetalle", query = "SELECT d FROM DetalleFactura d WHERE d.codigoDetalle = :codigoDetalle"),
    @NamedQuery(name = "DetalleFactura.findByCantidadProcedimiento", query = "SELECT d FROM DetalleFactura d WHERE d.cantidadProcedimiento = :cantidadProcedimiento"),
    @NamedQuery(name = "DetalleFactura.findByPrecioProcedimiento", query = "SELECT d FROM DetalleFactura d WHERE d.precioProcedimiento = :precioProcedimiento"),
    @NamedQuery(name = "DetalleFactura.findByPrecioARS", query = "SELECT d FROM DetalleFactura d WHERE d.precioARS = :precioARS"),
    @NamedQuery(name = "DetalleFactura.findByPrecioPaciente", query = "SELECT d FROM DetalleFactura d WHERE d.precioPaciente = :precioPaciente"),
    @NamedQuery(name = "DetalleFactura.findByDescuentoProcedimiento", query = "SELECT d FROM DetalleFactura d WHERE d.descuentoProcedimiento = :descuentoProcedimiento")})
public class DetalleFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "Aplica_Promocion")
    private String aplicaPromocion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Detalle")
    private BigDecimal codigoDetalle;
    @Size(max = 20)
    @Column(name = "Cantidad_Procedimiento")
    private String cantidadProcedimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio_Procedimiento")
    private BigDecimal precioProcedimiento;
    @Column(name = "Precio_ARS")
    private BigDecimal precioARS;
    @Column(name = "Precio_Paciente")
    private BigDecimal precioPaciente;
    @Column(name = "Descuento_Procedimiento")
    private BigDecimal descuentoProcedimiento;
    @JoinColumn(name = "Codigo_Factura", referencedColumnName = "Codigo_Factura")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EncabezadoFactura codigoFactura;
    @JoinColumn(name = "Codigo_Procedimiento", referencedColumnName = "Codigo_Procedimiento")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProcedimientoMedico codigoProcedimiento;
    @JoinColumn(name = "Codigo_Servicio", referencedColumnName = "Codigo_Servicio")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiciosMedicos codigoServicio;

    public DetalleFactura() {
    }

    public DetalleFactura(BigDecimal codigoDetalle) {
        this.codigoDetalle = codigoDetalle;
    }

    public DetalleFactura(BigDecimal codigoDetalle, String aplicaPromocion, BigDecimal precioProcedimiento) {
        this.codigoDetalle = codigoDetalle;
        this.aplicaPromocion = aplicaPromocion;
        this.precioProcedimiento = precioProcedimiento;
    }

    public String getAplicaPromocion() {
        return aplicaPromocion;
    }

    public void setAplicaPromocion(String aplicaPromocion) {
        this.aplicaPromocion = aplicaPromocion;
    }

    public BigDecimal getCodigoDetalle() {
        return codigoDetalle;
    }

    public void setCodigoDetalle(BigDecimal codigoDetalle) {
        this.codigoDetalle = codigoDetalle;
    }

    public String getCantidadProcedimiento() {
        return cantidadProcedimiento;
    }

    public void setCantidadProcedimiento(String cantidadProcedimiento) {
        this.cantidadProcedimiento = cantidadProcedimiento;
    }

    public BigDecimal getPrecioProcedimiento() {
        return precioProcedimiento;
    }

    public void setPrecioProcedimiento(BigDecimal precioProcedimiento) {
        this.precioProcedimiento = precioProcedimiento;
    }

    public BigDecimal getPrecioARS() {
        return precioARS;
    }

    public void setPrecioARS(BigDecimal precioARS) {
        this.precioARS = precioARS;
    }

    public BigDecimal getPrecioPaciente() {
        return precioPaciente;
    }

    public void setPrecioPaciente(BigDecimal precioPaciente) {
        this.precioPaciente = precioPaciente;
    }

    public BigDecimal getDescuentoProcedimiento() {
        return descuentoProcedimiento;
    }

    public void setDescuentoProcedimiento(BigDecimal descuentoProcedimiento) {
        this.descuentoProcedimiento = descuentoProcedimiento;
    }

    public EncabezadoFactura getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(EncabezadoFactura codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public ProcedimientoMedico getCodigoProcedimiento() {
        return codigoProcedimiento;
    }

    public void setCodigoProcedimiento(ProcedimientoMedico codigoProcedimiento) {
        this.codigoProcedimiento = codigoProcedimiento;
    }

    public ServiciosMedicos getCodigoServicio() {
        return codigoServicio;
    }

    public void setCodigoServicio(ServiciosMedicos codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoDetalle != null ? codigoDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleFactura)) {
            return false;
        }
        DetalleFactura other = (DetalleFactura) object;
        if ((this.codigoDetalle == null && other.codigoDetalle != null) || (this.codigoDetalle != null && !this.codigoDetalle.equals(other.codigoDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.DetalleFactura[ codigoDetalle=" + codigoDetalle + " ]";
    }
    
}
