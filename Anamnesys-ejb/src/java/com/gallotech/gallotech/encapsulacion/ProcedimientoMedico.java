/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Procedimiento_Medico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcedimientoMedico.findAll", query = "SELECT p FROM ProcedimientoMedico p"),
    @NamedQuery(name = "ProcedimientoMedico.findByCodigoProcedimiento", query = "SELECT p FROM ProcedimientoMedico p WHERE p.codigoProcedimiento = :codigoProcedimiento"),
    @NamedQuery(name = "ProcedimientoMedico.findByDescripcionProcedimiento", query = "SELECT p FROM ProcedimientoMedico p WHERE p.descripcionProcedimiento = :descripcionProcedimiento"),
    @NamedQuery(name = "ProcedimientoMedico.findByPrecioProcedimiento", query = "SELECT p FROM ProcedimientoMedico p WHERE p.precioProcedimiento = :precioProcedimiento")})
public class ProcedimientoMedico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Procedimiento")
    private Long codigoProcedimiento;
    @Size(max = 40)
    @Column(name = "Descripcion_Procedimiento")
    private String descripcionProcedimiento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Precio_Procedimiento")
    private BigDecimal precioProcedimiento;
    @OneToMany(mappedBy = "codigoProcedimiento", fetch = FetchType.LAZY)
    private List<DetalleFactura> detalleFacturaList;
    @JoinColumn(name = "Codigo_Especialidad", referencedColumnName = "Codigo_Especialidad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Especialidades codigoEspecialidad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "procedimientoMedico", fetch = FetchType.LAZY)
    private List<ProcedimientoARS> procedimientoARSList;

    public ProcedimientoMedico() {
    }

    public ProcedimientoMedico(Long codigoProcedimiento) {
        this.codigoProcedimiento = codigoProcedimiento;
    }

    public Long getCodigoProcedimiento() {
        return codigoProcedimiento;
    }

    public void setCodigoProcedimiento(Long codigoProcedimiento) {
        this.codigoProcedimiento = codigoProcedimiento;
    }

    public String getDescripcionProcedimiento() {
        return descripcionProcedimiento;
    }

    public void setDescripcionProcedimiento(String descripcionProcedimiento) {
        this.descripcionProcedimiento = descripcionProcedimiento;
    }

    public BigDecimal getPrecioProcedimiento() {
        return precioProcedimiento;
    }

    public void setPrecioProcedimiento(BigDecimal precioProcedimiento) {
        this.precioProcedimiento = precioProcedimiento;
    }

    @XmlTransient
    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    public Especialidades getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(Especialidades codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    @XmlTransient
    public List<ProcedimientoARS> getProcedimientoARSList() {
        return procedimientoARSList;
    }

    public void setProcedimientoARSList(List<ProcedimientoARS> procedimientoARSList) {
        this.procedimientoARSList = procedimientoARSList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoProcedimiento != null ? codigoProcedimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcedimientoMedico)) {
            return false;
        }
        ProcedimientoMedico other = (ProcedimientoMedico) object;
        if ((this.codigoProcedimiento == null && other.codigoProcedimiento != null) || (this.codigoProcedimiento != null && !this.codigoProcedimiento.equals(other.codigoProcedimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ProcedimientoMedico[ codigoProcedimiento=" + codigoProcedimiento + " ]";
    }
    
}
