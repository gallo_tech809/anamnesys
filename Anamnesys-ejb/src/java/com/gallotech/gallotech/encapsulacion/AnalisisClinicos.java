/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Analisis_Clinicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnalisisClinicos.findAll", query = "SELECT a FROM AnalisisClinicos a"),
    @NamedQuery(name = "AnalisisClinicos.findByCodigoAnalisisClinico", query = "SELECT a FROM AnalisisClinicos a WHERE a.codigoAnalisisClinico = :codigoAnalisisClinico"),
    @NamedQuery(name = "AnalisisClinicos.findByDescripcionAnalisisClinico", query = "SELECT a FROM AnalisisClinicos a WHERE a.descripcionAnalisisClinico = :descripcionAnalisisClinico")})
public class AnalisisClinicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Analisis_Clinico")
    private Long codigoAnalisisClinico;
    @Size(max = 128)
    @Column(name = "Descripcion_Analisis_Clinico")
    private String descripcionAnalisisClinico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "analisisClinicos", fetch = FetchType.LAZY)
    private List<AnalisisClinicoPaciente> analisisClinicoPacienteList;

    public AnalisisClinicos() {
    }

    public AnalisisClinicos(Long codigoAnalisisClinico) {
        this.codigoAnalisisClinico = codigoAnalisisClinico;
    }

    public Long getCodigoAnalisisClinico() {
        return codigoAnalisisClinico;
    }

    public void setCodigoAnalisisClinico(Long codigoAnalisisClinico) {
        this.codigoAnalisisClinico = codigoAnalisisClinico;
    }

    public String getDescripcionAnalisisClinico() {
        return descripcionAnalisisClinico;
    }

    public void setDescripcionAnalisisClinico(String descripcionAnalisisClinico) {
        this.descripcionAnalisisClinico = descripcionAnalisisClinico;
    }

    @XmlTransient
    public List<AnalisisClinicoPaciente> getAnalisisClinicoPacienteList() {
        return analisisClinicoPacienteList;
    }

    public void setAnalisisClinicoPacienteList(List<AnalisisClinicoPaciente> analisisClinicoPacienteList) {
        this.analisisClinicoPacienteList = analisisClinicoPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoAnalisisClinico != null ? codigoAnalisisClinico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalisisClinicos)) {
            return false;
        }
        AnalisisClinicos other = (AnalisisClinicos) object;
        if ((this.codigoAnalisisClinico == null && other.codigoAnalisisClinico != null) || (this.codigoAnalisisClinico != null && !this.codigoAnalisisClinico.equals(other.codigoAnalisisClinico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.AnalisisClinicos[ codigoAnalisisClinico=" + codigoAnalisisClinico + " ]";
    }
    
}
