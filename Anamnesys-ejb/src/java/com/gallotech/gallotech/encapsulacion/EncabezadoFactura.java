/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Encabezado_Factura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EncabezadoFactura.findAll", query = "SELECT e FROM EncabezadoFactura e"),
    @NamedQuery(name = "EncabezadoFactura.findByCodigoFactura", query = "SELECT e FROM EncabezadoFactura e WHERE e.codigoFactura = :codigoFactura"),
    @NamedQuery(name = "EncabezadoFactura.findByFechaFactura", query = "SELECT e FROM EncabezadoFactura e WHERE e.fechaFactura = :fechaFactura"),
    @NamedQuery(name = "EncabezadoFactura.findByMontoFactura", query = "SELECT e FROM EncabezadoFactura e WHERE e.montoFactura = :montoFactura"),
    @NamedQuery(name = "EncabezadoFactura.findByMontoARS", query = "SELECT e FROM EncabezadoFactura e WHERE e.montoARS = :montoARS"),
    @NamedQuery(name = "EncabezadoFactura.findByMontoPaciente", query = "SELECT e FROM EncabezadoFactura e WHERE e.montoPaciente = :montoPaciente")})
public class EncabezadoFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Factura")
    private BigDecimal codigoFactura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 27)
    @Column(name = "Fecha_Factura")
    private String fechaFactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Monto_Factura")
    private BigDecimal montoFactura;
    @Column(name = "Monto_ARS")
    private BigDecimal montoARS;
    @Column(name = "Monto_Paciente")
    private BigDecimal montoPaciente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoFactura", fetch = FetchType.LAZY)
    private List<DetalleFactura> detalleFacturaList;
    @JoinColumn(name = "Codigo_ARS", referencedColumnName = "Codigo_ARS")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ars codigoARS;
    @JoinColumn(name = "Codigo_Consultorio", referencedColumnName = "Codigo_Consultorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private Consultorios codigoConsultorio;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;

    public EncabezadoFactura() {
    }

    public EncabezadoFactura(BigDecimal codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public EncabezadoFactura(BigDecimal codigoFactura, String fechaFactura, BigDecimal montoFactura) {
        this.codigoFactura = codigoFactura;
        this.fechaFactura = fechaFactura;
        this.montoFactura = montoFactura;
    }

    public BigDecimal getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(BigDecimal codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public BigDecimal getMontoFactura() {
        return montoFactura;
    }

    public void setMontoFactura(BigDecimal montoFactura) {
        this.montoFactura = montoFactura;
    }

    public BigDecimal getMontoARS() {
        return montoARS;
    }

    public void setMontoARS(BigDecimal montoARS) {
        this.montoARS = montoARS;
    }

    public BigDecimal getMontoPaciente() {
        return montoPaciente;
    }

    public void setMontoPaciente(BigDecimal montoPaciente) {
        this.montoPaciente = montoPaciente;
    }

    @XmlTransient
    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    public Ars getCodigoARS() {
        return codigoARS;
    }

    public void setCodigoARS(Ars codigoARS) {
        this.codigoARS = codigoARS;
    }

    public Consultorios getCodigoConsultorio() {
        return codigoConsultorio;
    }

    public void setCodigoConsultorio(Consultorios codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoFactura != null ? codigoFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EncabezadoFactura)) {
            return false;
        }
        EncabezadoFactura other = (EncabezadoFactura) object;
        if ((this.codigoFactura == null && other.codigoFactura != null) || (this.codigoFactura != null && !this.codigoFactura.equals(other.codigoFactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.EncabezadoFactura[ codigoFactura=" + codigoFactura + " ]";
    }
    
}
