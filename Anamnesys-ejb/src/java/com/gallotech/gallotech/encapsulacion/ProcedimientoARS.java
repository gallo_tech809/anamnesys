/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Procedimiento_ARS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcedimientoARS.findAll", query = "SELECT p FROM ProcedimientoARS p"),
    @NamedQuery(name = "ProcedimientoARS.findByCodigoARS", query = "SELECT p FROM ProcedimientoARS p WHERE p.procedimientoARSPK.codigoARS = :codigoARS"),
    @NamedQuery(name = "ProcedimientoARS.findByCodigoProcedimiento", query = "SELECT p FROM ProcedimientoARS p WHERE p.procedimientoARSPK.codigoProcedimiento = :codigoProcedimiento"),
    @NamedQuery(name = "ProcedimientoARS.findByPrecioARS", query = "SELECT p FROM ProcedimientoARS p WHERE p.precioARS = :precioARS")})
public class ProcedimientoARS implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProcedimientoARSPK procedimientoARSPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio_ARS")
    private BigDecimal precioARS;
    @JoinColumn(name = "Codigo_ARS", referencedColumnName = "Codigo_ARS", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ars ars;
    @JoinColumn(name = "Codigo_Especialidad", referencedColumnName = "Codigo_Especialidad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Especialidades codigoEspecialidad;
    @JoinColumn(name = "Codigo_Procedimiento", referencedColumnName = "Codigo_Procedimiento", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ProcedimientoMedico procedimientoMedico;

    public ProcedimientoARS() {
    }

    public ProcedimientoARS(ProcedimientoARSPK procedimientoARSPK) {
        this.procedimientoARSPK = procedimientoARSPK;
    }

    public ProcedimientoARS(ProcedimientoARSPK procedimientoARSPK, BigDecimal precioARS) {
        this.procedimientoARSPK = procedimientoARSPK;
        this.precioARS = precioARS;
    }

    public ProcedimientoARS(long codigoARS, long codigoProcedimiento) {
        this.procedimientoARSPK = new ProcedimientoARSPK(codigoARS, codigoProcedimiento);
    }

    public ProcedimientoARSPK getProcedimientoARSPK() {
        return procedimientoARSPK;
    }

    public void setProcedimientoARSPK(ProcedimientoARSPK procedimientoARSPK) {
        this.procedimientoARSPK = procedimientoARSPK;
    }

    public BigDecimal getPrecioARS() {
        return precioARS;
    }

    public void setPrecioARS(BigDecimal precioARS) {
        this.precioARS = precioARS;
    }

    public Ars getArs() {
        return ars;
    }

    public void setArs(Ars ars) {
        this.ars = ars;
    }

    public Especialidades getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(Especialidades codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public ProcedimientoMedico getProcedimientoMedico() {
        return procedimientoMedico;
    }

    public void setProcedimientoMedico(ProcedimientoMedico procedimientoMedico) {
        this.procedimientoMedico = procedimientoMedico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (procedimientoARSPK != null ? procedimientoARSPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcedimientoARS)) {
            return false;
        }
        ProcedimientoARS other = (ProcedimientoARS) object;
        if ((this.procedimientoARSPK == null && other.procedimientoARSPK != null) || (this.procedimientoARSPK != null && !this.procedimientoARSPK.equals(other.procedimientoARSPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ProcedimientoARS[ procedimientoARSPK=" + procedimientoARSPK + " ]";
    }
    
}
