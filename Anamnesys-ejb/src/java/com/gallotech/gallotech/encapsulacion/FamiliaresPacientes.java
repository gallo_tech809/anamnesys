/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Familiares_Pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FamiliaresPacientes.findAll", query = "SELECT f FROM FamiliaresPacientes f"),
    @NamedQuery(name = "FamiliaresPacientes.findByTipoParentesco", query = "SELECT f FROM FamiliaresPacientes f WHERE f.tipoParentesco = :tipoParentesco"),
    @NamedQuery(name = "FamiliaresPacientes.findByNombreFamiliar", query = "SELECT f FROM FamiliaresPacientes f WHERE f.nombreFamiliar = :nombreFamiliar"),
    @NamedQuery(name = "FamiliaresPacientes.findByApellidoFamiliar", query = "SELECT f FROM FamiliaresPacientes f WHERE f.apellidoFamiliar = :apellidoFamiliar"),
    @NamedQuery(name = "FamiliaresPacientes.findByTipoSangre", query = "SELECT f FROM FamiliaresPacientes f WHERE f.tipoSangre = :tipoSangre"),
    @NamedQuery(name = "FamiliaresPacientes.findByDireccionFamiliar", query = "SELECT f FROM FamiliaresPacientes f WHERE f.direccionFamiliar = :direccionFamiliar"),
    @NamedQuery(name = "FamiliaresPacientes.findByCodigoFamiliarPaciente", query = "SELECT f FROM FamiliaresPacientes f WHERE f.codigoFamiliarPaciente = :codigoFamiliarPaciente")})
public class FamiliaresPacientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Tipo_Parentesco")
    private String tipoParentesco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "Nombre_Familiar")
    private String nombreFamiliar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "Apellido_Familiar")
    private String apellidoFamiliar;
    @Size(max = 4)
    @Column(name = "Tipo_Sangre")
    private String tipoSangre;
    @Size(max = 1024)
    @Column(name = "Direccion_Familiar")
    private String direccionFamiliar;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Familiar_Paciente")
    private BigDecimal codigoFamiliarPaciente;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;
    @OneToMany(mappedBy = "codigoFamiliarPaciente", fetch = FetchType.LAZY)
    private List<MedioComunicacionFamiliar> medioComunicacionFamiliarList;

    public FamiliaresPacientes() {
    }

    public FamiliaresPacientes(BigDecimal codigoFamiliarPaciente) {
        this.codigoFamiliarPaciente = codigoFamiliarPaciente;
    }

    public FamiliaresPacientes(BigDecimal codigoFamiliarPaciente, String tipoParentesco, String nombreFamiliar, String apellidoFamiliar) {
        this.codigoFamiliarPaciente = codigoFamiliarPaciente;
        this.tipoParentesco = tipoParentesco;
        this.nombreFamiliar = nombreFamiliar;
        this.apellidoFamiliar = apellidoFamiliar;
    }

    public String getTipoParentesco() {
        return tipoParentesco;
    }

    public void setTipoParentesco(String tipoParentesco) {
        this.tipoParentesco = tipoParentesco;
    }

    public String getNombreFamiliar() {
        return nombreFamiliar;
    }

    public void setNombreFamiliar(String nombreFamiliar) {
        this.nombreFamiliar = nombreFamiliar;
    }

    public String getApellidoFamiliar() {
        return apellidoFamiliar;
    }

    public void setApellidoFamiliar(String apellidoFamiliar) {
        this.apellidoFamiliar = apellidoFamiliar;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public String getDireccionFamiliar() {
        return direccionFamiliar;
    }

    public void setDireccionFamiliar(String direccionFamiliar) {
        this.direccionFamiliar = direccionFamiliar;
    }

    public BigDecimal getCodigoFamiliarPaciente() {
        return codigoFamiliarPaciente;
    }

    public void setCodigoFamiliarPaciente(BigDecimal codigoFamiliarPaciente) {
        this.codigoFamiliarPaciente = codigoFamiliarPaciente;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @XmlTransient
    public List<MedioComunicacionFamiliar> getMedioComunicacionFamiliarList() {
        return medioComunicacionFamiliarList;
    }

    public void setMedioComunicacionFamiliarList(List<MedioComunicacionFamiliar> medioComunicacionFamiliarList) {
        this.medioComunicacionFamiliarList = medioComunicacionFamiliarList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoFamiliarPaciente != null ? codigoFamiliarPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FamiliaresPacientes)) {
            return false;
        }
        FamiliaresPacientes other = (FamiliaresPacientes) object;
        if ((this.codigoFamiliarPaciente == null && other.codigoFamiliarPaciente != null) || (this.codigoFamiliarPaciente != null && !this.codigoFamiliarPaciente.equals(other.codigoFamiliarPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.FamiliaresPacientes[ codigoFamiliarPaciente=" + codigoFamiliarPaciente + " ]";
    }
    
}
