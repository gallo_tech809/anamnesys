/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Medicamentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medicamentos.findAll", query = "SELECT m FROM Medicamentos m"),
    @NamedQuery(name = "Medicamentos.findByCodigoMedicamento", query = "SELECT m FROM Medicamentos m WHERE m.codigoMedicamento = :codigoMedicamento"),
    @NamedQuery(name = "Medicamentos.findByDescripcionMedicamento", query = "SELECT m FROM Medicamentos m WHERE m.descripcionMedicamento = :descripcionMedicamento"),
    @NamedQuery(name = "Medicamentos.findByPresentacionMedicamento", query = "SELECT m FROM Medicamentos m WHERE m.presentacionMedicamento = :presentacionMedicamento"),
    @NamedQuery(name = "Medicamentos.findByInstruccionMedicamento", query = "SELECT m FROM Medicamentos m WHERE m.instruccionMedicamento = :instruccionMedicamento")})
public class Medicamentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Medicamento")
    private Long codigoMedicamento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Medicamento")
    private String descripcionMedicamento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Presentacion_Medicamento")
    private String presentacionMedicamento;
    @Size(max = 256)
    @Column(name = "Instruccion_Medicamento")
    private String instruccionMedicamento;
    @ManyToMany(mappedBy = "medicamentosList", fetch = FetchType.LAZY)
    private List<Laboratorio> laboratorioList;
    @JoinColumn(name = "Codigo_Laboratorio", referencedColumnName = "Codigo_Laboratorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private Laboratorio codigoLaboratorio;
    @OneToMany(mappedBy = "codigoMedicamento", fetch = FetchType.LAZY)
    private List<RecetaPaciente> recetaPacienteList;

    public Medicamentos() {
    }

    public Medicamentos(Long codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    public Medicamentos(Long codigoMedicamento, String descripcionMedicamento, String presentacionMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
        this.descripcionMedicamento = descripcionMedicamento;
        this.presentacionMedicamento = presentacionMedicamento;
    }

    public Long getCodigoMedicamento() {
        return codigoMedicamento;
    }

    public void setCodigoMedicamento(Long codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    public String getDescripcionMedicamento() {
        return descripcionMedicamento;
    }

    public void setDescripcionMedicamento(String descripcionMedicamento) {
        this.descripcionMedicamento = descripcionMedicamento;
    }

    public String getPresentacionMedicamento() {
        return presentacionMedicamento;
    }

    public void setPresentacionMedicamento(String presentacionMedicamento) {
        this.presentacionMedicamento = presentacionMedicamento;
    }

    public String getInstruccionMedicamento() {
        return instruccionMedicamento;
    }

    public void setInstruccionMedicamento(String instruccionMedicamento) {
        this.instruccionMedicamento = instruccionMedicamento;
    }

    @XmlTransient
    public List<Laboratorio> getLaboratorioList() {
        return laboratorioList;
    }

    public void setLaboratorioList(List<Laboratorio> laboratorioList) {
        this.laboratorioList = laboratorioList;
    }

    public Laboratorio getCodigoLaboratorio() {
        return codigoLaboratorio;
    }

    public void setCodigoLaboratorio(Laboratorio codigoLaboratorio) {
        this.codigoLaboratorio = codigoLaboratorio;
    }

    @XmlTransient
    public List<RecetaPaciente> getRecetaPacienteList() {
        return recetaPacienteList;
    }

    public void setRecetaPacienteList(List<RecetaPaciente> recetaPacienteList) {
        this.recetaPacienteList = recetaPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoMedicamento != null ? codigoMedicamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medicamentos)) {
            return false;
        }
        Medicamentos other = (Medicamentos) object;
        if ((this.codigoMedicamento == null && other.codigoMedicamento != null) || (this.codigoMedicamento != null && !this.codigoMedicamento.equals(other.codigoMedicamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Medicamentos[ codigoMedicamento=" + codigoMedicamento + " ]";
    }
    
}
