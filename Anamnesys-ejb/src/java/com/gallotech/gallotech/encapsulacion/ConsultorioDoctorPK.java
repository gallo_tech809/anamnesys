/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alvasrey
 */
@Embeddable
public class ConsultorioDoctorPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Doctor")
    private long codigoDoctor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Consultorio")
    private long codigoConsultorio;

    public ConsultorioDoctorPK() {
    }

    public ConsultorioDoctorPK(long codigoDoctor, long codigoConsultorio) {
        this.codigoDoctor = codigoDoctor;
        this.codigoConsultorio = codigoConsultorio;
    }

    public long getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(long codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public long getCodigoConsultorio() {
        return codigoConsultorio;
    }

    public void setCodigoConsultorio(long codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoDoctor;
        hash += (int) codigoConsultorio;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultorioDoctorPK)) {
            return false;
        }
        ConsultorioDoctorPK other = (ConsultorioDoctorPK) object;
        if (this.codigoDoctor != other.codigoDoctor) {
            return false;
        }
        if (this.codigoConsultorio != other.codigoConsultorio) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ConsultorioDoctorPK[ codigoDoctor=" + codigoDoctor + ", codigoConsultorio=" + codigoConsultorio + " ]";
    }

}
