/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pacientes.findAll", query = "SELECT p FROM Pacientes p"),
    @NamedQuery(name = "Pacientes.findByCodigoPaciente", query = "SELECT p FROM Pacientes p WHERE p.codigoPaciente = :codigoPaciente"),
    @NamedQuery(name = "Pacientes.findByPrimerNombre", query = "SELECT p FROM Pacientes p WHERE p.primerNombre = :primerNombre"),
    @NamedQuery(name = "Pacientes.findBySegundoNombre", query = "SELECT p FROM Pacientes p WHERE p.segundoNombre = :segundoNombre"),
    @NamedQuery(name = "Pacientes.findByPrimerApellido", query = "SELECT p FROM Pacientes p WHERE p.primerApellido = :primerApellido"),
    @NamedQuery(name = "Pacientes.findBySegundoApellido", query = "SELECT p FROM Pacientes p WHERE p.segundoApellido = :segundoApellido"),
    @NamedQuery(name = "Pacientes.findBySexo", query = "SELECT p FROM Pacientes p WHERE p.sexo = :sexo"),
    @NamedQuery(name = "Pacientes.findByFechaNacimiento", query = "SELECT p FROM Pacientes p WHERE p.fechaNacimiento = :fechaNacimiento"),
    @NamedQuery(name = "Pacientes.findByEstadoCivil", query = "SELECT p FROM Pacientes p WHERE p.estadoCivil = :estadoCivil"),
    @NamedQuery(name = "Pacientes.findByOcupacion", query = "SELECT p FROM Pacientes p WHERE p.ocupacion = :ocupacion"),
    @NamedQuery(name = "Pacientes.findByDireccionPrincipal", query = "SELECT p FROM Pacientes p WHERE p.direccionPrincipal = :direccionPrincipal"),
    @NamedQuery(name = "Pacientes.findByReligion", query = "SELECT p FROM Pacientes p WHERE p.religion = :religion"),
    @NamedQuery(name = "Pacientes.findByTipoSanguineo", query = "SELECT p FROM Pacientes p WHERE p.tipoSanguineo = :tipoSanguineo")})
public class Pacientes implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Paciente")
    private BigDecimal codigoPaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Primer_Nombre")
    private String primerNombre;
    @Size(max = 40)
    @Column(name = "Segundo_Nombre")
    private String segundoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Primer_Apellido")
    private String primerApellido;
    @Size(max = 40)
    @Column(name = "Segundo_Apellido")
    private String segundoApellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "Sexo")
    private String sexo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Nacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Size(max = 20)
    @Column(name = "Estado_Civil")
    private String estadoCivil;
    @Size(max = 20)
    @Column(name = "Ocupacion")
    private String ocupacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "Direccion_Principal")
    private String direccionPrincipal;
    @Size(max = 20)
    @Column(name = "Religion")
    private String religion;
    @Size(max = 3)
    @Column(name = "Tipo_Sanguineo")
    private String tipoSanguineo;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<CitasPaciente> citasPacienteList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<EnfermedadesPacientes> enfermedadesPacientesList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<FamiliaresPacientes> familiaresPacientesList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<Consulta> consultaList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<ImagenesPaciente> imagenesPacienteList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<EncabezadoFactura> encabezadoFacturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<EstudioClinicoPaciente> estudioClinicoPacienteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacientes", fetch = FetchType.LAZY)
    private List<AnalisisClinicoPaciente> analisisClinicoPacienteList;
    @JoinColumn(name = "Codigo_ARS", referencedColumnName = "Codigo_ARS")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ars codigoARS;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Pais", referencedColumnName = "Codigo_Pais")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pais codigoPais;
    @JoinColumn(name = "Codigo_Provincia", referencedColumnName = "Codigo_Provincia")
    @ManyToOne(fetch = FetchType.LAZY)
    private Provincia codigoProvincia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacientes", fetch = FetchType.LAZY)
    private List<OcurrenciaEvaluacionPaciente> ocurrenciaEvaluacionPacienteList;
    @OneToMany(mappedBy = "codigoPaciente", fetch = FetchType.LAZY)
    private List<MedioComunicacionPaciente> medioComunicacionPacienteList;

    public Pacientes() {
    }

    public Pacientes(BigDecimal codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    public Pacientes(BigDecimal codigoPaciente, String primerNombre, String primerApellido, String sexo, Date fechaNacimiento, String direccionPrincipal) {
        this.codigoPaciente = codigoPaciente;
        this.primerNombre = primerNombre;
        this.primerApellido = primerApellido;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.direccionPrincipal = direccionPrincipal;
    }

    public BigDecimal getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(BigDecimal codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    @XmlTransient
    public List<CitasPaciente> getCitasPacienteList() {
        return citasPacienteList;
    }

    public void setCitasPacienteList(List<CitasPaciente> citasPacienteList) {
        this.citasPacienteList = citasPacienteList;
    }

    @XmlTransient
    public List<EnfermedadesPacientes> getEnfermedadesPacientesList() {
        return enfermedadesPacientesList;
    }

    public void setEnfermedadesPacientesList(List<EnfermedadesPacientes> enfermedadesPacientesList) {
        this.enfermedadesPacientesList = enfermedadesPacientesList;
    }

    @XmlTransient
    public List<FamiliaresPacientes> getFamiliaresPacientesList() {
        return familiaresPacientesList;
    }

    public void setFamiliaresPacientesList(List<FamiliaresPacientes> familiaresPacientesList) {
        this.familiaresPacientesList = familiaresPacientesList;
    }

    @XmlTransient
    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    @XmlTransient
    public List<ImagenesPaciente> getImagenesPacienteList() {
        return imagenesPacienteList;
    }

    public void setImagenesPacienteList(List<ImagenesPaciente> imagenesPacienteList) {
        this.imagenesPacienteList = imagenesPacienteList;
    }

    @XmlTransient
    public List<EncabezadoFactura> getEncabezadoFacturaList() {
        return encabezadoFacturaList;
    }

    public void setEncabezadoFacturaList(List<EncabezadoFactura> encabezadoFacturaList) {
        this.encabezadoFacturaList = encabezadoFacturaList;
    }

    @XmlTransient
    public List<EstudioClinicoPaciente> getEstudioClinicoPacienteList() {
        return estudioClinicoPacienteList;
    }

    public void setEstudioClinicoPacienteList(List<EstudioClinicoPaciente> estudioClinicoPacienteList) {
        this.estudioClinicoPacienteList = estudioClinicoPacienteList;
    }

    @XmlTransient
    public List<AnalisisClinicoPaciente> getAnalisisClinicoPacienteList() {
        return analisisClinicoPacienteList;
    }

    public void setAnalisisClinicoPacienteList(List<AnalisisClinicoPaciente> analisisClinicoPacienteList) {
        this.analisisClinicoPacienteList = analisisClinicoPacienteList;
    }

    public Ars getCodigoARS() {
        return codigoARS;
    }

    public void setCodigoARS(Ars codigoARS) {
        this.codigoARS = codigoARS;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Pais getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(Pais codigoPais) {
        this.codigoPais = codigoPais;
    }

    public Provincia getCodigoProvincia() {
        return codigoProvincia;
    }

    public void setCodigoProvincia(Provincia codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }

    @XmlTransient
    public List<OcurrenciaEvaluacionPaciente> getOcurrenciaEvaluacionPacienteList() {
        return ocurrenciaEvaluacionPacienteList;
    }

    public void setOcurrenciaEvaluacionPacienteList(List<OcurrenciaEvaluacionPaciente> ocurrenciaEvaluacionPacienteList) {
        this.ocurrenciaEvaluacionPacienteList = ocurrenciaEvaluacionPacienteList;
    }

    @XmlTransient
    public List<MedioComunicacionPaciente> getMedioComunicacionPacienteList() {
        return medioComunicacionPacienteList;
    }

    public void setMedioComunicacionPacienteList(List<MedioComunicacionPaciente> medioComunicacionPacienteList) {
        this.medioComunicacionPacienteList = medioComunicacionPacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoPaciente != null ? codigoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacientes)) {
            return false;
        }
        Pacientes other = (Pacientes) object;
        if ((this.codigoPaciente == null && other.codigoPaciente != null) || (this.codigoPaciente != null && !this.codigoPaciente.equals(other.codigoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Pacientes[ codigoPaciente=" + codigoPaciente + " ]";
    }
    
}
