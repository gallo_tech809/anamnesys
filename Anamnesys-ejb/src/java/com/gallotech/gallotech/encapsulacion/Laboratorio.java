/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Laboratorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Laboratorio.findAll", query = "SELECT l FROM Laboratorio l"),
    @NamedQuery(name = "Laboratorio.findByCodigoLaboratorio", query = "SELECT l FROM Laboratorio l WHERE l.codigoLaboratorio = :codigoLaboratorio"),
    @NamedQuery(name = "Laboratorio.findByDescripcionLaboratorio", query = "SELECT l FROM Laboratorio l WHERE l.descripcionLaboratorio = :descripcionLaboratorio")})
public class Laboratorio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Laboratorio")
    private Long codigoLaboratorio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Laboratorio")
    private String descripcionLaboratorio;
    @JoinTable(name = "Laboratorio_Medicamento", joinColumns = {
        @JoinColumn(name = "Codigo_Laboratorio", referencedColumnName = "Codigo_Laboratorio")}, inverseJoinColumns = {
        @JoinColumn(name = "Codigo_Medicamento", referencedColumnName = "Codigo_Medicamento")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Medicamentos> medicamentosList;
    @OneToMany(mappedBy = "codigoLaboratorio", fetch = FetchType.LAZY)
    private List<MedioComunicacionLaboratorio> medioComunicacionLaboratorioList;
    @OneToMany(mappedBy = "codigoLaboratorio", fetch = FetchType.LAZY)
    private List<Medicamentos> medicamentosList1;

    public Laboratorio() {
    }

    public Laboratorio(Long codigoLaboratorio) {
        this.codigoLaboratorio = codigoLaboratorio;
    }

    public Laboratorio(Long codigoLaboratorio, String descripcionLaboratorio) {
        this.codigoLaboratorio = codigoLaboratorio;
        this.descripcionLaboratorio = descripcionLaboratorio;
    }

    public Long getCodigoLaboratorio() {
        return codigoLaboratorio;
    }

    public void setCodigoLaboratorio(Long codigoLaboratorio) {
        this.codigoLaboratorio = codigoLaboratorio;
    }

    public String getDescripcionLaboratorio() {
        return descripcionLaboratorio;
    }

    public void setDescripcionLaboratorio(String descripcionLaboratorio) {
        this.descripcionLaboratorio = descripcionLaboratorio;
    }

    @XmlTransient
    public List<Medicamentos> getMedicamentosList() {
        return medicamentosList;
    }

    public void setMedicamentosList(List<Medicamentos> medicamentosList) {
        this.medicamentosList = medicamentosList;
    }

    @XmlTransient
    public List<MedioComunicacionLaboratorio> getMedioComunicacionLaboratorioList() {
        return medioComunicacionLaboratorioList;
    }

    public void setMedioComunicacionLaboratorioList(List<MedioComunicacionLaboratorio> medioComunicacionLaboratorioList) {
        this.medioComunicacionLaboratorioList = medioComunicacionLaboratorioList;
    }

    @XmlTransient
    public List<Medicamentos> getMedicamentosList1() {
        return medicamentosList1;
    }

    public void setMedicamentosList1(List<Medicamentos> medicamentosList1) {
        this.medicamentosList1 = medicamentosList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoLaboratorio != null ? codigoLaboratorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Laboratorio)) {
            return false;
        }
        Laboratorio other = (Laboratorio) object;
        if ((this.codigoLaboratorio == null && other.codigoLaboratorio != null) || (this.codigoLaboratorio != null && !this.codigoLaboratorio.equals(other.codigoLaboratorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Laboratorio[ codigoLaboratorio=" + codigoLaboratorio + " ]";
    }
    
}
