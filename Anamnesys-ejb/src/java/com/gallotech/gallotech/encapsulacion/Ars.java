/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "ARS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ars.findAll", query = "SELECT a FROM Ars a"),
    @NamedQuery(name = "Ars.findByCodigoARS", query = "SELECT a FROM Ars a WHERE a.codigoARS = :codigoARS"),
    @NamedQuery(name = "Ars.findByDescripcionARS", query = "SELECT a FROM Ars a WHERE a.descripcionARS = :descripcionARS")})
public class Ars implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_ARS")
    private Long codigoARS;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "Descripcion_ARS")
    private String descripcionARS;
    @OneToMany(mappedBy = "codigoARS", fetch = FetchType.LAZY)
    private List<Consulta> consultaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ars", fetch = FetchType.LAZY)
    private List<ProcedimientoARS> procedimientoARSList;
    @OneToMany(mappedBy = "codigoARS", fetch = FetchType.LAZY)
    private List<EncabezadoFactura> encabezadoFacturaList;
    @OneToMany(mappedBy = "codigoARS", fetch = FetchType.LAZY)
    private List<Pacientes> pacientesList;

    public Ars() {
    }

    public Ars(Long codigoARS) {
        this.codigoARS = codigoARS;
    }

    public Ars(Long codigoARS, String descripcionARS) {
        this.codigoARS = codigoARS;
        this.descripcionARS = descripcionARS;
    }

    public Long getCodigoARS() {
        return codigoARS;
    }

    public void setCodigoARS(Long codigoARS) {
        this.codigoARS = codigoARS;
    }

    public String getDescripcionARS() {
        return descripcionARS;
    }

    public void setDescripcionARS(String descripcionARS) {
        this.descripcionARS = descripcionARS;
    }

    @XmlTransient
    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    @XmlTransient
    public List<ProcedimientoARS> getProcedimientoARSList() {
        return procedimientoARSList;
    }

    public void setProcedimientoARSList(List<ProcedimientoARS> procedimientoARSList) {
        this.procedimientoARSList = procedimientoARSList;
    }

    @XmlTransient
    public List<EncabezadoFactura> getEncabezadoFacturaList() {
        return encabezadoFacturaList;
    }

    public void setEncabezadoFacturaList(List<EncabezadoFactura> encabezadoFacturaList) {
        this.encabezadoFacturaList = encabezadoFacturaList;
    }

    @XmlTransient
    public List<Pacientes> getPacientesList() {
        return pacientesList;
    }

    public void setPacientesList(List<Pacientes> pacientesList) {
        this.pacientesList = pacientesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoARS != null ? codigoARS.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ars)) {
            return false;
        }
        Ars other = (Ars) object;
        if ((this.codigoARS == null && other.codigoARS != null) || (this.codigoARS != null && !this.codigoARS.equals(other.codigoARS))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Ars[ codigoARS=" + codigoARS + " ]";
    }
    
}
