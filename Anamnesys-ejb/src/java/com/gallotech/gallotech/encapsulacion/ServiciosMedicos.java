/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Servicios_Medicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiciosMedicos.findAll", query = "SELECT s FROM ServiciosMedicos s"),
    @NamedQuery(name = "ServiciosMedicos.findByCodigoServicio", query = "SELECT s FROM ServiciosMedicos s WHERE s.codigoServicio = :codigoServicio"),
    @NamedQuery(name = "ServiciosMedicos.findByDescripcionServicio", query = "SELECT s FROM ServiciosMedicos s WHERE s.descripcionServicio = :descripcionServicio"),
    @NamedQuery(name = "ServiciosMedicos.findByPrecioServicio", query = "SELECT s FROM ServiciosMedicos s WHERE s.precioServicio = :precioServicio"),
    @NamedQuery(name = "ServiciosMedicos.findByDescuentoServicio", query = "SELECT s FROM ServiciosMedicos s WHERE s.descuentoServicio = :descuentoServicio")})
public class ServiciosMedicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Servicio")
    private Long codigoServicio;
    @Size(max = 1024)
    @Column(name = "Descripcion_Servicio")
    private String descripcionServicio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Precio_Servicio")
    private BigDecimal precioServicio;
    @Column(name = "Descuento_Servicio")
    private BigDecimal descuentoServicio;
    @OneToMany(mappedBy = "codigoServicio", fetch = FetchType.LAZY)
    private List<DetalleFactura> detalleFacturaList;

    public ServiciosMedicos() {
    }

    public ServiciosMedicos(Long codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public Long getCodigoServicio() {
        return codigoServicio;
    }

    public void setCodigoServicio(Long codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public String getDescripcionServicio() {
        return descripcionServicio;
    }

    public void setDescripcionServicio(String descripcionServicio) {
        this.descripcionServicio = descripcionServicio;
    }

    public BigDecimal getPrecioServicio() {
        return precioServicio;
    }

    public void setPrecioServicio(BigDecimal precioServicio) {
        this.precioServicio = precioServicio;
    }

    public BigDecimal getDescuentoServicio() {
        return descuentoServicio;
    }

    public void setDescuentoServicio(BigDecimal descuentoServicio) {
        this.descuentoServicio = descuentoServicio;
    }

    @XmlTransient
    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoServicio != null ? codigoServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiciosMedicos)) {
            return false;
        }
        ServiciosMedicos other = (ServiciosMedicos) object;
        if ((this.codigoServicio == null && other.codigoServicio != null) || (this.codigoServicio != null && !this.codigoServicio.equals(other.codigoServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ServiciosMedicos[ codigoServicio=" + codigoServicio + " ]";
    }
    
}
