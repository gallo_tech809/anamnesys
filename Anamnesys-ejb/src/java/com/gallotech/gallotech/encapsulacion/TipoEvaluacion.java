/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Tipo_Evaluacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEvaluacion.findAll", query = "SELECT t FROM TipoEvaluacion t"),
    @NamedQuery(name = "TipoEvaluacion.findByCodigoTipoEvaluacion", query = "SELECT t FROM TipoEvaluacion t WHERE t.codigoTipoEvaluacion = :codigoTipoEvaluacion"),
    @NamedQuery(name = "TipoEvaluacion.findByDescripcionTipoEvaluacion", query = "SELECT t FROM TipoEvaluacion t WHERE t.descripcionTipoEvaluacion = :descripcionTipoEvaluacion")})
public class TipoEvaluacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Tipo_Evaluacion")
    private Long codigoTipoEvaluacion;
    @Size(max = 40)
    @Column(name = "Descripcion_Tipo_Evaluacion")
    private String descripcionTipoEvaluacion;
    @OneToMany(mappedBy = "codigoTipoEvaluacion", fetch = FetchType.LAZY)
    private List<Evaluaciones> evaluacionesList;

    public TipoEvaluacion() {
    }

    public TipoEvaluacion(Long codigoTipoEvaluacion) {
        this.codigoTipoEvaluacion = codigoTipoEvaluacion;
    }

    public Long getCodigoTipoEvaluacion() {
        return codigoTipoEvaluacion;
    }

    public void setCodigoTipoEvaluacion(Long codigoTipoEvaluacion) {
        this.codigoTipoEvaluacion = codigoTipoEvaluacion;
    }

    public String getDescripcionTipoEvaluacion() {
        return descripcionTipoEvaluacion;
    }

    public void setDescripcionTipoEvaluacion(String descripcionTipoEvaluacion) {
        this.descripcionTipoEvaluacion = descripcionTipoEvaluacion;
    }

    @XmlTransient
    public List<Evaluaciones> getEvaluacionesList() {
        return evaluacionesList;
    }

    public void setEvaluacionesList(List<Evaluaciones> evaluacionesList) {
        this.evaluacionesList = evaluacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoTipoEvaluacion != null ? codigoTipoEvaluacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEvaluacion)) {
            return false;
        }
        TipoEvaluacion other = (TipoEvaluacion) object;
        if ((this.codigoTipoEvaluacion == null && other.codigoTipoEvaluacion != null) || (this.codigoTipoEvaluacion != null && !this.codigoTipoEvaluacion.equals(other.codigoTipoEvaluacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.TipoEvaluacion[ codigoTipoEvaluacion=" + codigoTipoEvaluacion + " ]";
    }
    
}
