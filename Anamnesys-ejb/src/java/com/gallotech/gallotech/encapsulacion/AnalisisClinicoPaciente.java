/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Analisis_Clinico_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnalisisClinicoPaciente.findAll", query = "SELECT a FROM AnalisisClinicoPaciente a"),
    @NamedQuery(name = "AnalisisClinicoPaciente.findByCodigoAnalisisClinico", query = "SELECT a FROM AnalisisClinicoPaciente a WHERE a.analisisClinicoPacientePK.codigoAnalisisClinico = :codigoAnalisisClinico"),
    @NamedQuery(name = "AnalisisClinicoPaciente.findByCodigoPaciente", query = "SELECT a FROM AnalisisClinicoPaciente a WHERE a.analisisClinicoPacientePK.codigoPaciente = :codigoPaciente"),
    @NamedQuery(name = "AnalisisClinicoPaciente.findByResultadoAnalisisClinico", query = "SELECT a FROM AnalisisClinicoPaciente a WHERE a.resultadoAnalisisClinico = :resultadoAnalisisClinico")})
public class AnalisisClinicoPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnalisisClinicoPacientePK analisisClinicoPacientePK;
    @Size(max = 256)
    @Column(name = "Resultado_Analisis_Clinico")
    private String resultadoAnalisisClinico;
    @JoinColumn(name = "Codigo_Analisis_Clinico", referencedColumnName = "Codigo_Analisis_Clinico", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AnalisisClinicos analisisClinicos;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Pacientes pacientes;

    public AnalisisClinicoPaciente() {
    }

    public AnalisisClinicoPaciente(AnalisisClinicoPacientePK analisisClinicoPacientePK) {
        this.analisisClinicoPacientePK = analisisClinicoPacientePK;
    }

    public AnalisisClinicoPaciente(long codigoAnalisisClinico, BigInteger codigoPaciente) {
        this.analisisClinicoPacientePK = new AnalisisClinicoPacientePK(codigoAnalisisClinico, codigoPaciente);
    }

    public AnalisisClinicoPacientePK getAnalisisClinicoPacientePK() {
        return analisisClinicoPacientePK;
    }

    public void setAnalisisClinicoPacientePK(AnalisisClinicoPacientePK analisisClinicoPacientePK) {
        this.analisisClinicoPacientePK = analisisClinicoPacientePK;
    }

    public String getResultadoAnalisisClinico() {
        return resultadoAnalisisClinico;
    }

    public void setResultadoAnalisisClinico(String resultadoAnalisisClinico) {
        this.resultadoAnalisisClinico = resultadoAnalisisClinico;
    }

    public AnalisisClinicos getAnalisisClinicos() {
        return analisisClinicos;
    }

    public void setAnalisisClinicos(AnalisisClinicos analisisClinicos) {
        this.analisisClinicos = analisisClinicos;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Pacientes getPacientes() {
        return pacientes;
    }

    public void setPacientes(Pacientes pacientes) {
        this.pacientes = pacientes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analisisClinicoPacientePK != null ? analisisClinicoPacientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalisisClinicoPaciente)) {
            return false;
        }
        AnalisisClinicoPaciente other = (AnalisisClinicoPaciente) object;
        if ((this.analisisClinicoPacientePK == null && other.analisisClinicoPacientePK != null) || (this.analisisClinicoPacientePK != null && !this.analisisClinicoPacientePK.equals(other.analisisClinicoPacientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.AnalisisClinicoPaciente[ analisisClinicoPacientePK=" + analisisClinicoPacientePK + " ]";
    }
    
}
