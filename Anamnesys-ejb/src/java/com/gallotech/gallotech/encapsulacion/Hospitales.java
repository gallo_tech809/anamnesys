/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Hospitales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hospitales.findAll", query = "SELECT h FROM Hospitales h"),
    @NamedQuery(name = "Hospitales.findByCodigoHospital", query = "SELECT h FROM Hospitales h WHERE h.codigoHospital = :codigoHospital"),
    @NamedQuery(name = "Hospitales.findByDescripcionHospital", query = "SELECT h FROM Hospitales h WHERE h.descripcionHospital = :descripcionHospital")})
public class Hospitales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Hospital")
    private Long codigoHospital;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripcion_Hospital")
    private String descripcionHospital;
    @OneToMany(mappedBy = "codigoHospital", fetch = FetchType.LAZY)
    private List<Consultorios> consultoriosList;

    public Hospitales() {
    }

    public Hospitales(Long codigoHospital) {
        this.codigoHospital = codigoHospital;
    }

    public Hospitales(Long codigoHospital, String descripcionHospital) {
        this.codigoHospital = codigoHospital;
        this.descripcionHospital = descripcionHospital;
    }

    public Long getCodigoHospital() {
        return codigoHospital;
    }

    public void setCodigoHospital(Long codigoHospital) {
        this.codigoHospital = codigoHospital;
    }

    public String getDescripcionHospital() {
        return descripcionHospital;
    }

    public void setDescripcionHospital(String descripcionHospital) {
        this.descripcionHospital = descripcionHospital;
    }

    @XmlTransient
    public List<Consultorios> getConsultoriosList() {
        return consultoriosList;
    }

    public void setConsultoriosList(List<Consultorios> consultoriosList) {
        this.consultoriosList = consultoriosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoHospital != null ? codigoHospital.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hospitales)) {
            return false;
        }
        Hospitales other = (Hospitales) object;
        if ((this.codigoHospital == null && other.codigoHospital != null) || (this.codigoHospital != null && !this.codigoHospital.equals(other.codigoHospital))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Hospitales[ codigoHospital=" + codigoHospital + " ]";
    }
    
}
