/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alvasrey
 */
@Embeddable
public class AnalisisClinicoPacientePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Analisis_Clinico")
    private long codigoAnalisisClinico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Paciente")
    private BigInteger codigoPaciente;

    public AnalisisClinicoPacientePK() {
    }

    public AnalisisClinicoPacientePK(long codigoAnalisisClinico, BigInteger codigoPaciente) {
        this.codigoAnalisisClinico = codigoAnalisisClinico;
        this.codigoPaciente = codigoPaciente;
    }

    public long getCodigoAnalisisClinico() {
        return codigoAnalisisClinico;
    }

    public void setCodigoAnalisisClinico(long codigoAnalisisClinico) {
        this.codigoAnalisisClinico = codigoAnalisisClinico;
    }

    public BigInteger getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(BigInteger codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoAnalisisClinico;
        hash += (codigoPaciente != null ? codigoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnalisisClinicoPacientePK)) {
            return false;
        }
        AnalisisClinicoPacientePK other = (AnalisisClinicoPacientePK) object;
        if (this.codigoAnalisisClinico != other.codigoAnalisisClinico) {
            return false;
        }
        if ((this.codigoPaciente == null && other.codigoPaciente != null) || (this.codigoPaciente != null && !this.codigoPaciente.equals(other.codigoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.AnalisisClinicoPacientePK[ codigoAnalisisClinico=" + codigoAnalisisClinico + ", codigoPaciente=" + codigoPaciente + " ]";
    }
    
}
