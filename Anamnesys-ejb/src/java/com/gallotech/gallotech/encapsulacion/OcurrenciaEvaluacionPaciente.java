/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Ocurrencia_Evaluacion_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OcurrenciaEvaluacionPaciente.findAll", query = "SELECT o FROM OcurrenciaEvaluacionPaciente o"),
    @NamedQuery(name = "OcurrenciaEvaluacionPaciente.findByCodigoEvaluacion", query = "SELECT o FROM OcurrenciaEvaluacionPaciente o WHERE o.ocurrenciaEvaluacionPacientePK.codigoEvaluacion = :codigoEvaluacion"),
    @NamedQuery(name = "OcurrenciaEvaluacionPaciente.findByCodigoPaciente", query = "SELECT o FROM OcurrenciaEvaluacionPaciente o WHERE o.ocurrenciaEvaluacionPacientePK.codigoPaciente = :codigoPaciente"),
    @NamedQuery(name = "OcurrenciaEvaluacionPaciente.findByNotaEvaluacion", query = "SELECT o FROM OcurrenciaEvaluacionPaciente o WHERE o.notaEvaluacion = :notaEvaluacion")})
public class OcurrenciaEvaluacionPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OcurrenciaEvaluacionPacientePK ocurrenciaEvaluacionPacientePK;
    @Size(max = 2048)
    @Column(name = "Nota_Evaluacion")
    private String notaEvaluacion;
    @JoinColumn(name = "Codigo_Evaluacion", referencedColumnName = "Codigo_Evaluacion", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evaluaciones evaluaciones;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Pacientes pacientes;

    public OcurrenciaEvaluacionPaciente() {
    }

    public OcurrenciaEvaluacionPaciente(OcurrenciaEvaluacionPacientePK ocurrenciaEvaluacionPacientePK) {
        this.ocurrenciaEvaluacionPacientePK = ocurrenciaEvaluacionPacientePK;
    }

    public OcurrenciaEvaluacionPaciente(BigInteger codigoEvaluacion, BigInteger codigoPaciente) {
        this.ocurrenciaEvaluacionPacientePK = new OcurrenciaEvaluacionPacientePK(codigoEvaluacion, codigoPaciente);
    }

    public OcurrenciaEvaluacionPacientePK getOcurrenciaEvaluacionPacientePK() {
        return ocurrenciaEvaluacionPacientePK;
    }

    public void setOcurrenciaEvaluacionPacientePK(OcurrenciaEvaluacionPacientePK ocurrenciaEvaluacionPacientePK) {
        this.ocurrenciaEvaluacionPacientePK = ocurrenciaEvaluacionPacientePK;
    }

    public String getNotaEvaluacion() {
        return notaEvaluacion;
    }

    public void setNotaEvaluacion(String notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }

    public Evaluaciones getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(Evaluaciones evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public Pacientes getPacientes() {
        return pacientes;
    }

    public void setPacientes(Pacientes pacientes) {
        this.pacientes = pacientes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ocurrenciaEvaluacionPacientePK != null ? ocurrenciaEvaluacionPacientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OcurrenciaEvaluacionPaciente)) {
            return false;
        }
        OcurrenciaEvaluacionPaciente other = (OcurrenciaEvaluacionPaciente) object;
        if ((this.ocurrenciaEvaluacionPacientePK == null && other.ocurrenciaEvaluacionPacientePK != null) || (this.ocurrenciaEvaluacionPacientePK != null && !this.ocurrenciaEvaluacionPacientePK.equals(other.ocurrenciaEvaluacionPacientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.OcurrenciaEvaluacionPaciente[ ocurrenciaEvaluacionPacientePK=" + ocurrenciaEvaluacionPacientePK + " ]";
    }
    
}
