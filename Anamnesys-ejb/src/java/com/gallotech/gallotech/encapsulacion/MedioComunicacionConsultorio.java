/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Medio_Comunicacion_Consultorio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedioComunicacionConsultorio.findAll", query = "SELECT m FROM MedioComunicacionConsultorio m"),
    @NamedQuery(name = "MedioComunicacionConsultorio.findByCodigoMedioComunicacionConsultorio", query = "SELECT m FROM MedioComunicacionConsultorio m WHERE m.codigoMedioComunicacionConsultorio = :codigoMedioComunicacionConsultorio"),
    @NamedQuery(name = "MedioComunicacionConsultorio.findByDescripcion", query = "SELECT m FROM MedioComunicacionConsultorio m WHERE m.descripcion = :descripcion")})
public class MedioComunicacionConsultorio implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Medio_Comunicacion_Consultorio")
    private BigDecimal codigoMedioComunicacionConsultorio;
    @Size(max = 20)
    @Column(name = "Descripcion")
    private String descripcion;
    @JoinColumn(name = "Codigo_Consultorio", referencedColumnName = "Codigo_Consultorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private Consultorios codigoConsultorio;
    @OneToMany(mappedBy = "fKCodigoMedioComunicacionConsultorio", fetch = FetchType.LAZY)
    private List<MedioComunicacionConsultorio> medioComunicacionConsultorioList;
    @JoinColumn(name = "FK_Codigo_Medio_Comunicacion_Consultorio", referencedColumnName = "Codigo_Medio_Comunicacion_Consultorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private MedioComunicacionConsultorio fKCodigoMedioComunicacionConsultorio;

    public MedioComunicacionConsultorio() {
    }

    public MedioComunicacionConsultorio(BigDecimal codigoMedioComunicacionConsultorio) {
        this.codigoMedioComunicacionConsultorio = codigoMedioComunicacionConsultorio;
    }

    public BigDecimal getCodigoMedioComunicacionConsultorio() {
        return codigoMedioComunicacionConsultorio;
    }

    public void setCodigoMedioComunicacionConsultorio(BigDecimal codigoMedioComunicacionConsultorio) {
        this.codigoMedioComunicacionConsultorio = codigoMedioComunicacionConsultorio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Consultorios getCodigoConsultorio() {
        return codigoConsultorio;
    }

    public void setCodigoConsultorio(Consultorios codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    @XmlTransient
    public List<MedioComunicacionConsultorio> getMedioComunicacionConsultorioList() {
        return medioComunicacionConsultorioList;
    }

    public void setMedioComunicacionConsultorioList(List<MedioComunicacionConsultorio> medioComunicacionConsultorioList) {
        this.medioComunicacionConsultorioList = medioComunicacionConsultorioList;
    }

    public MedioComunicacionConsultorio getFKCodigoMedioComunicacionConsultorio() {
        return fKCodigoMedioComunicacionConsultorio;
    }

    public void setFKCodigoMedioComunicacionConsultorio(MedioComunicacionConsultorio fKCodigoMedioComunicacionConsultorio) {
        this.fKCodigoMedioComunicacionConsultorio = fKCodigoMedioComunicacionConsultorio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoMedioComunicacionConsultorio != null ? codigoMedioComunicacionConsultorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioComunicacionConsultorio)) {
            return false;
        }
        MedioComunicacionConsultorio other = (MedioComunicacionConsultorio) object;
        if ((this.codigoMedioComunicacionConsultorio == null && other.codigoMedioComunicacionConsultorio != null) || (this.codigoMedioComunicacionConsultorio != null && !this.codigoMedioComunicacionConsultorio.equals(other.codigoMedioComunicacionConsultorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.MedioComunicacionConsultorio[ codigoMedioComunicacionConsultorio=" + codigoMedioComunicacionConsultorio + " ]";
    }
    
}
