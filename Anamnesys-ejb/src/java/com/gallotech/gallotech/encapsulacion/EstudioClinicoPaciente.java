/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Estudio_Clinico_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstudioClinicoPaciente.findAll", query = "SELECT e FROM EstudioClinicoPaciente e"),
    @NamedQuery(name = "EstudioClinicoPaciente.findByCodigoEstudioClinicoPaciente", query = "SELECT e FROM EstudioClinicoPaciente e WHERE e.codigoEstudioClinicoPaciente = :codigoEstudioClinicoPaciente"),
    @NamedQuery(name = "EstudioClinicoPaciente.findByFechaEstudio", query = "SELECT e FROM EstudioClinicoPaciente e WHERE e.fechaEstudio = :fechaEstudio"),
    @NamedQuery(name = "EstudioClinicoPaciente.findByFechaEntrega", query = "SELECT e FROM EstudioClinicoPaciente e WHERE e.fechaEntrega = :fechaEntrega"),
    @NamedQuery(name = "EstudioClinicoPaciente.findByResultado", query = "SELECT e FROM EstudioClinicoPaciente e WHERE e.resultado = :resultado")})
public class EstudioClinicoPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Estudio_Clinico_Paciente")
    private BigDecimal codigoEstudioClinicoPaciente;
    @Size(max = 27)
    @Column(name = "Fecha_Estudio")
    private String fechaEstudio;
    @Size(max = 27)
    @Column(name = "Fecha_Entrega")
    private String fechaEntrega;
    @Size(max = 128)
    @Column(name = "Resultado")
    private String resultado;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Estudio_Clinico", referencedColumnName = "Codigo_Estudio_Clinico")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EstudiosClinicos codigoEstudioClinico;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;

    public EstudioClinicoPaciente() {
    }

    public EstudioClinicoPaciente(BigDecimal codigoEstudioClinicoPaciente) {
        this.codigoEstudioClinicoPaciente = codigoEstudioClinicoPaciente;
    }

    public BigDecimal getCodigoEstudioClinicoPaciente() {
        return codigoEstudioClinicoPaciente;
    }

    public void setCodigoEstudioClinicoPaciente(BigDecimal codigoEstudioClinicoPaciente) {
        this.codigoEstudioClinicoPaciente = codigoEstudioClinicoPaciente;
    }

    public String getFechaEstudio() {
        return fechaEstudio;
    }

    public void setFechaEstudio(String fechaEstudio) {
        this.fechaEstudio = fechaEstudio;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public EstudiosClinicos getCodigoEstudioClinico() {
        return codigoEstudioClinico;
    }

    public void setCodigoEstudioClinico(EstudiosClinicos codigoEstudioClinico) {
        this.codigoEstudioClinico = codigoEstudioClinico;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEstudioClinicoPaciente != null ? codigoEstudioClinicoPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudioClinicoPaciente)) {
            return false;
        }
        EstudioClinicoPaciente other = (EstudioClinicoPaciente) object;
        if ((this.codigoEstudioClinicoPaciente == null && other.codigoEstudioClinicoPaciente != null) || (this.codigoEstudioClinicoPaciente != null && !this.codigoEstudioClinicoPaciente.equals(other.codigoEstudioClinicoPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.EstudioClinicoPaciente[ codigoEstudioClinicoPaciente=" + codigoEstudioClinicoPaciente + " ]";
    }
    
}
