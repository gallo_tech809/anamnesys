/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alvasrey
 */
@Embeddable
public class ProcedimientoARSPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_ARS")
    private long codigoARS;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Procedimiento")
    private long codigoProcedimiento;

    public ProcedimientoARSPK() {
    }

    public ProcedimientoARSPK(long codigoARS, long codigoProcedimiento) {
        this.codigoARS = codigoARS;
        this.codigoProcedimiento = codigoProcedimiento;
    }

    public long getCodigoARS() {
        return codigoARS;
    }

    public void setCodigoARS(long codigoARS) {
        this.codigoARS = codigoARS;
    }

    public long getCodigoProcedimiento() {
        return codigoProcedimiento;
    }

    public void setCodigoProcedimiento(long codigoProcedimiento) {
        this.codigoProcedimiento = codigoProcedimiento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoARS;
        hash += (int) codigoProcedimiento;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcedimientoARSPK)) {
            return false;
        }
        ProcedimientoARSPK other = (ProcedimientoARSPK) object;
        if (this.codigoARS != other.codigoARS) {
            return false;
        }
        if (this.codigoProcedimiento != other.codigoProcedimiento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.ProcedimientoARSPK[ codigoARS=" + codigoARS + ", codigoProcedimiento=" + codigoProcedimiento + " ]";
    }
    
}
