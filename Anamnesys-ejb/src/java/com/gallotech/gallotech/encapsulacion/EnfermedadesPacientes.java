/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Enfermedades_Pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnfermedadesPacientes.findAll", query = "SELECT e FROM EnfermedadesPacientes e"),
    @NamedQuery(name = "EnfermedadesPacientes.findByCodigoEnfermedadPaciente", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.codigoEnfermedadPaciente = :codigoEnfermedadPaciente"),
    @NamedQuery(name = "EnfermedadesPacientes.findBySeveridadEnfermedad", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.severidadEnfermedad = :severidadEnfermedad"),
    @NamedQuery(name = "EnfermedadesPacientes.findByEstadoEnfermedad", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.estadoEnfermedad = :estadoEnfermedad"),
    @NamedQuery(name = "EnfermedadesPacientes.findByEnfermedadActiva", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.enfermedadActiva = :enfermedadActiva"),
    @NamedQuery(name = "EnfermedadesPacientes.findByFechaDiagnostico", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.fechaDiagnostico = :fechaDiagnostico"),
    @NamedQuery(name = "EnfermedadesPacientes.findByFechaCurada", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.fechaCurada = :fechaCurada"),
    @NamedQuery(name = "EnfermedadesPacientes.findByEnfermedadAlergica", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.enfermedadAlergica = :enfermedadAlergica"),
    @NamedQuery(name = "EnfermedadesPacientes.findByTipoAlergia", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.tipoAlergia = :tipoAlergia"),
    @NamedQuery(name = "EnfermedadesPacientes.findByObservaciones", query = "SELECT e FROM EnfermedadesPacientes e WHERE e.observaciones = :observaciones")})
public class EnfermedadesPacientes implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Enfermedad_Paciente")
    private BigDecimal codigoEnfermedadPaciente;
    @Size(max = 60)
    @Column(name = "Severidad_Enfermedad")
    private String severidadEnfermedad;
    @Size(max = 60)
    @Column(name = "Estado_Enfermedad")
    private String estadoEnfermedad;
    @Size(max = 1)
    @Column(name = "Enfermedad_Activa")
    private String enfermedadActiva;
    @Column(name = "Fecha_Diagnostico")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDiagnostico;
    @Column(name = "Fecha_Curada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCurada;
    @Size(max = 1)
    @Column(name = "Enfermedad_Alergica")
    private String enfermedadAlergica;
    @Size(max = 60)
    @Column(name = "Tipo_Alergia")
    private String tipoAlergia;
    @Size(max = 2048)
    @Column(name = "Observaciones")
    private String observaciones;
    @JoinColumn(name = "Codigo_Consulta", referencedColumnName = "Codigo_Consulta")
    @ManyToOne(fetch = FetchType.LAZY)
    private Consulta codigoConsulta;
    @JoinColumn(name = "Codigo_Enfermedad", referencedColumnName = "Codigo_Enfermedad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Enfermedades codigoEnfermedad;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;

    public EnfermedadesPacientes() {
    }

    public EnfermedadesPacientes(BigDecimal codigoEnfermedadPaciente) {
        this.codigoEnfermedadPaciente = codigoEnfermedadPaciente;
    }

    public BigDecimal getCodigoEnfermedadPaciente() {
        return codigoEnfermedadPaciente;
    }

    public void setCodigoEnfermedadPaciente(BigDecimal codigoEnfermedadPaciente) {
        this.codigoEnfermedadPaciente = codigoEnfermedadPaciente;
    }

    public String getSeveridadEnfermedad() {
        return severidadEnfermedad;
    }

    public void setSeveridadEnfermedad(String severidadEnfermedad) {
        this.severidadEnfermedad = severidadEnfermedad;
    }

    public String getEstadoEnfermedad() {
        return estadoEnfermedad;
    }

    public void setEstadoEnfermedad(String estadoEnfermedad) {
        this.estadoEnfermedad = estadoEnfermedad;
    }

    public String getEnfermedadActiva() {
        return enfermedadActiva;
    }

    public void setEnfermedadActiva(String enfermedadActiva) {
        this.enfermedadActiva = enfermedadActiva;
    }

    public Date getFechaDiagnostico() {
        return fechaDiagnostico;
    }

    public void setFechaDiagnostico(Date fechaDiagnostico) {
        this.fechaDiagnostico = fechaDiagnostico;
    }

    public Date getFechaCurada() {
        return fechaCurada;
    }

    public void setFechaCurada(Date fechaCurada) {
        this.fechaCurada = fechaCurada;
    }

    public String getEnfermedadAlergica() {
        return enfermedadAlergica;
    }

    public void setEnfermedadAlergica(String enfermedadAlergica) {
        this.enfermedadAlergica = enfermedadAlergica;
    }

    public String getTipoAlergia() {
        return tipoAlergia;
    }

    public void setTipoAlergia(String tipoAlergia) {
        this.tipoAlergia = tipoAlergia;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Consulta getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(Consulta codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

    public Enfermedades getCodigoEnfermedad() {
        return codigoEnfermedad;
    }

    public void setCodigoEnfermedad(Enfermedades codigoEnfermedad) {
        this.codigoEnfermedad = codigoEnfermedad;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEnfermedadPaciente != null ? codigoEnfermedadPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EnfermedadesPacientes)) {
            return false;
        }
        EnfermedadesPacientes other = (EnfermedadesPacientes) object;
        if ((this.codigoEnfermedadPaciente == null && other.codigoEnfermedadPaciente != null) || (this.codigoEnfermedadPaciente != null && !this.codigoEnfermedadPaciente.equals(other.codigoEnfermedadPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.EnfermedadesPacientes[ codigoEnfermedadPaciente=" + codigoEnfermedadPaciente + " ]";
    }
    
}
