/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Doctores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Doctores.findAll", query = "SELECT d FROM Doctores d"),
    @NamedQuery(name = "Doctores.findByCodigoDoctor", query = "SELECT d FROM Doctores d WHERE d.codigoDoctor = :codigoDoctor"),
    @NamedQuery(name = "Doctores.findByNombreDoctor", query = "SELECT d FROM Doctores d WHERE d.nombreDoctor = :nombreDoctor"),
    @NamedQuery(name = "Doctores.findByApellidoDoctor", query = "SELECT d FROM Doctores d WHERE d.apellidoDoctor = :apellidoDoctor"),
    @NamedQuery(name = "Doctores.findByExecuaturDoctor", query = "SELECT d FROM Doctores d WHERE d.execuaturDoctor = :execuaturDoctor")})
public class Doctores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Doctor")
    private Long codigoDoctor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Nombre_Doctor")
    private String nombreDoctor;
    @Size(max = 40)
    @Column(name = "Apellido_Doctor")
    private String apellidoDoctor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Execuatur_Doctor")
    private String execuaturDoctor;
    @OneToMany(mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<CitasPaciente> citasPacienteList;
    @JoinColumn(name = "Codigo_Especialidad", referencedColumnName = "Codigo_Especialidad")
    @ManyToOne(fetch = FetchType.LAZY)
    private Especialidades codigoEspecialidad;
    @JoinColumn(name = "Codigo_Usuario", referencedColumnName = "Codigo_Usuario")
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuarios codigoUsuario;
    @OneToMany(mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<Consulta> consultaList;
    @OneToMany(mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<EncabezadoFactura> encabezadoFacturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctores", fetch = FetchType.LAZY)
    private List<ConsultorioDoctor> consultorioDoctorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<EstudioClinicoPaciente> estudioClinicoPacienteList;
    @OneToMany(mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<AnalisisClinicoPaciente> analisisClinicoPacienteList;
    @OneToMany(mappedBy = "codigoDoctor", fetch = FetchType.LAZY)
    private List<Pacientes> pacientesList;

    public Doctores() {
    }

    public Doctores(Long codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Doctores(Long codigoDoctor, String nombreDoctor, String execuaturDoctor) {
        this.codigoDoctor = codigoDoctor;
        this.nombreDoctor = nombreDoctor;
        this.execuaturDoctor = execuaturDoctor;
    }

    public Long getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Long codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public String getNombreDoctor() {
        return nombreDoctor;
    }

    public void setNombreDoctor(String nombreDoctor) {
        this.nombreDoctor = nombreDoctor;
    }

    public String getApellidoDoctor() {
        return apellidoDoctor;
    }

    public void setApellidoDoctor(String apellidoDoctor) {
        this.apellidoDoctor = apellidoDoctor;
    }

    public String getExecuaturDoctor() {
        return execuaturDoctor;
    }

    public void setExecuaturDoctor(String execuaturDoctor) {
        this.execuaturDoctor = execuaturDoctor;
    }

    @XmlTransient
    public List<CitasPaciente> getCitasPacienteList() {
        return citasPacienteList;
    }

    public void setCitasPacienteList(List<CitasPaciente> citasPacienteList) {
        this.citasPacienteList = citasPacienteList;
    }

    public Especialidades getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(Especialidades codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public Usuarios getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Usuarios codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    @XmlTransient
    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    @XmlTransient
    public List<EncabezadoFactura> getEncabezadoFacturaList() {
        return encabezadoFacturaList;
    }

    public void setEncabezadoFacturaList(List<EncabezadoFactura> encabezadoFacturaList) {
        this.encabezadoFacturaList = encabezadoFacturaList;
    }

    @XmlTransient
    public List<ConsultorioDoctor> getConsultorioDoctorList() {
        return consultorioDoctorList;
    }

    public void setConsultorioDoctorList(List<ConsultorioDoctor> consultorioDoctorList) {
        this.consultorioDoctorList = consultorioDoctorList;
    }

    @XmlTransient
    public List<EstudioClinicoPaciente> getEstudioClinicoPacienteList() {
        return estudioClinicoPacienteList;
    }

    public void setEstudioClinicoPacienteList(List<EstudioClinicoPaciente> estudioClinicoPacienteList) {
        this.estudioClinicoPacienteList = estudioClinicoPacienteList;
    }

    @XmlTransient
    public List<AnalisisClinicoPaciente> getAnalisisClinicoPacienteList() {
        return analisisClinicoPacienteList;
    }

    public void setAnalisisClinicoPacienteList(List<AnalisisClinicoPaciente> analisisClinicoPacienteList) {
        this.analisisClinicoPacienteList = analisisClinicoPacienteList;
    }

    @XmlTransient
    public List<Pacientes> getPacientesList() {
        return pacientesList;
    }

    public void setPacientesList(List<Pacientes> pacientesList) {
        this.pacientesList = pacientesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoDoctor != null ? codigoDoctor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doctores)) {
            return false;
        }
        Doctores other = (Doctores) object;
        if ((this.codigoDoctor == null && other.codigoDoctor != null) || (this.codigoDoctor != null && !this.codigoDoctor.equals(other.codigoDoctor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.Doctores[ codigoDoctor=" + codigoDoctor + " ]";
    }
    
}
