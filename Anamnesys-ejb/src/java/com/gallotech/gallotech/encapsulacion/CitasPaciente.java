/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Citas_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CitasPaciente.findAll", query = "SELECT c FROM CitasPaciente c"),
    @NamedQuery(name = "CitasPaciente.findByCodigoCita", query = "SELECT c FROM CitasPaciente c WHERE c.codigoCita = :codigoCita"),
    @NamedQuery(name = "CitasPaciente.findByFechaCita", query = "SELECT c FROM CitasPaciente c WHERE c.fechaCita = :fechaCita"),
    @NamedQuery(name = "CitasPaciente.findByEstadoCita", query = "SELECT c FROM CitasPaciente c WHERE c.estadoCita = :estadoCita"),
    @NamedQuery(name = "CitasPaciente.findByObservacion", query = "SELECT c FROM CitasPaciente c WHERE c.observacion = :observacion")})
public class CitasPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Cita")
    private Long codigoCita;
    @Column(name = "Fecha_Cita")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCita;
    @Size(max = 1)
    @Column(name = "Estado_Cita")
    private String estadoCita;
    @Size(max = 2048)
    @Column(name = "Observacion")
    private String observacion;
    @JoinColumn(name = "Codigo_Consultorio", referencedColumnName = "Codigo_Consultorio")
    @ManyToOne(fetch = FetchType.LAZY)
    private Consultorios codigoConsultorio;
    @JoinColumn(name = "Codigo_Doctor", referencedColumnName = "Codigo_Doctor")
    @ManyToOne(fetch = FetchType.LAZY)
    private Doctores codigoDoctor;
    @JoinColumn(name = "Codigo_Paciente", referencedColumnName = "Codigo_Paciente")
    @ManyToOne(fetch = FetchType.LAZY)
    private Pacientes codigoPaciente;

    public CitasPaciente() {
    }

    public CitasPaciente(Long codigoCita) {
        this.codigoCita = codigoCita;
    }

    public Long getCodigoCita() {
        return codigoCita;
    }

    public void setCodigoCita(Long codigoCita) {
        this.codigoCita = codigoCita;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getEstadoCita() {
        return estadoCita;
    }

    public void setEstadoCita(String estadoCita) {
        this.estadoCita = estadoCita;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Consultorios getCodigoConsultorio() {
        return codigoConsultorio;
    }

    public void setCodigoConsultorio(Consultorios codigoConsultorio) {
        this.codigoConsultorio = codigoConsultorio;
    }

    public Doctores getCodigoDoctor() {
        return codigoDoctor;
    }

    public void setCodigoDoctor(Doctores codigoDoctor) {
        this.codigoDoctor = codigoDoctor;
    }

    public Pacientes getCodigoPaciente() {
        return codigoPaciente;
    }

    public void setCodigoPaciente(Pacientes codigoPaciente) {
        this.codigoPaciente = codigoPaciente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoCita != null ? codigoCita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CitasPaciente)) {
            return false;
        }
        CitasPaciente other = (CitasPaciente) object;
        if ((this.codigoCita == null && other.codigoCita != null) || (this.codigoCita != null && !this.codigoCita.equals(other.codigoCita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.CitasPaciente[ codigoCita=" + codigoCita + " ]";
    }
    
}
