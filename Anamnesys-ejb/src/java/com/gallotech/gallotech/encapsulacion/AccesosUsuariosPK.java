/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alvasrey
 */
@Embeddable
public class AccesosUsuariosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Usuario")
    private long codigoUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Nivel")
    private long codigoNivel;

    public AccesosUsuariosPK() {
    }

    public AccesosUsuariosPK(long codigoUsuario, long codigoNivel) {
        this.codigoUsuario = codigoUsuario;
        this.codigoNivel = codigoNivel;
    }

    public long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public long getCodigoNivel() {
        return codigoNivel;
    }

    public void setCodigoNivel(long codigoNivel) {
        this.codigoNivel = codigoNivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoUsuario;
        hash += (int) codigoNivel;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccesosUsuariosPK)) {
            return false;
        }
        AccesosUsuariosPK other = (AccesosUsuariosPK) object;
        if (this.codigoUsuario != other.codigoUsuario) {
            return false;
        }
        if (this.codigoNivel != other.codigoNivel) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.AccesosUsuariosPK[ codigoUsuario=" + codigoUsuario + ", codigoNivel=" + codigoNivel + " ]";
    }
    
}
