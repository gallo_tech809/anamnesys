/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.encapsulacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvasrey
 */
@Entity
@Table(name = "Receta_Paciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RecetaPaciente.findAll", query = "SELECT r FROM RecetaPaciente r"),
    @NamedQuery(name = "RecetaPaciente.findByCodigoReceta", query = "SELECT r FROM RecetaPaciente r WHERE r.codigoReceta = :codigoReceta"),
    @NamedQuery(name = "RecetaPaciente.findByObservacion", query = "SELECT r FROM RecetaPaciente r WHERE r.observacion = :observacion")})
public class RecetaPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Codigo_Receta")
    private Long codigoReceta;
    @Size(max = 2048)
    @Column(name = "Observacion")
    private String observacion;
    @JoinColumn(name = "Codigo_Consulta", referencedColumnName = "Codigo_Consulta")
    @ManyToOne(fetch = FetchType.LAZY)
    private Consulta codigoConsulta;
    @JoinColumn(name = "Codigo_Medicamento", referencedColumnName = "Codigo_Medicamento")
    @ManyToOne(fetch = FetchType.LAZY)
    private Medicamentos codigoMedicamento;

    public RecetaPaciente() {
    }

    public RecetaPaciente(Long codigoReceta) {
        this.codigoReceta = codigoReceta;
    }

    public Long getCodigoReceta() {
        return codigoReceta;
    }

    public void setCodigoReceta(Long codigoReceta) {
        this.codigoReceta = codigoReceta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Consulta getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(Consulta codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

    public Medicamentos getCodigoMedicamento() {
        return codigoMedicamento;
    }

    public void setCodigoMedicamento(Medicamentos codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoReceta != null ? codigoReceta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecetaPaciente)) {
            return false;
        }
        RecetaPaciente other = (RecetaPaciente) object;
        if ((this.codigoReceta == null && other.codigoReceta != null) || (this.codigoReceta != null && !this.codigoReceta.equals(other.codigoReceta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gallotech.gallotech.encapsulacion.RecetaPaciente[ codigoReceta=" + codigoReceta + " ]";
    }
    
}
