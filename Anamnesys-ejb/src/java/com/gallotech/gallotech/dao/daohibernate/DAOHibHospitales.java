/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOHospitales;
import com.gallotech.gallotech.encapsulacion.Hospitales;

public class DAOHibHospitales extends GenericHibernateDAO<Hospitales, Long> implements DAOHospitales{

    @Override
    public Hospitales getHospitalByCodigoHospital(Long parCodigoHospital) {
        return this.buscarPorID(parCodigoHospital);
    }



    
}
