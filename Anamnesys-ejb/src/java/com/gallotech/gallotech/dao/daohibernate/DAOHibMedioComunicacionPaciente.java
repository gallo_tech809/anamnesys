/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.encapsulacion.MedioComunicacionPaciente;
import java.math.BigDecimal;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibMedioComunicacionPaciente extends GenericHibernateDAO<MedioComunicacionPaciente, BigDecimal> {
    
}
