/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOEvaluaciones;
import com.gallotech.gallotech.encapsulacion.Evaluaciones;
import java.math.BigDecimal;

public class DAOHibEvaluaciones extends GenericHibernateDAO<Evaluaciones, BigDecimal> implements DAOEvaluaciones{

    @Override
    public Evaluaciones getEvaluacionByCodigoEvaluacion(BigDecimal parCodigoEvaluacion) {
        return this.buscarPorID(parCodigoEvaluacion);
    }



    
}
