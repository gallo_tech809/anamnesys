/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */


import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOServiciosMedicos;
import com.gallotech.gallotech.encapsulacion.ServiciosMedicos;
/**
 *
 * @author Bayardo Mejia
 */
public class DAOHibServiciosMedicos extends GenericHibernateDAO<ServiciosMedicos, Long> implements DAOServiciosMedicos{

    @Override
    public ServiciosMedicos getServicioByCodigoServicio(Long parCodigoServicio) {
        return this.buscarPorID(parCodigoServicio);
    }


    
}