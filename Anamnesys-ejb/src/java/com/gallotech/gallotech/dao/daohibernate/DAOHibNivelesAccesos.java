/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAONivelesAccesos;
import com.gallotech.gallotech.encapsulacion.NivelesAccesos;

public class DAOHibNivelesAccesos extends GenericHibernateDAO<NivelesAccesos, Long> implements DAONivelesAccesos{

    @Override
    public NivelesAccesos getNivelByCodigoNivel(Long parCodigoNivel) {
        return this.buscarPorID(parCodigoNivel);
    }



    
}
