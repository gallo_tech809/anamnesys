/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOEspecialidades;
import com.gallotech.gallotech.encapsulacion.Especialidades;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibEspecialidades extends GenericHibernateDAO<Especialidades, Long>implements DAOEspecialidades {

    @Override
    public Especialidades getEspecialidadesByCodigoEspecialidades(Long parCodigoEspecialidades) {
        return this.buscarPorID(parCodigoEspecialidades);
    }
    
}
