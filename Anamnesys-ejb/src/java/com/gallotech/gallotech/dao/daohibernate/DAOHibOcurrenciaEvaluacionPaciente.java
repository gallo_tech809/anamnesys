/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.encapsulacion.OcurrenciaEvaluacionPaciente;
import com.gallotech.gallotech.encapsulacion.OcurrenciaEvaluacionPacientePK;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibOcurrenciaEvaluacionPaciente extends GenericHibernateDAO<OcurrenciaEvaluacionPaciente, OcurrenciaEvaluacionPacientePK>{
    
}
