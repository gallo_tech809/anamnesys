/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOPais;
import com.gallotech.gallotech.encapsulacion.Pais;

public class DAOHibPais extends GenericHibernateDAO<Pais, Long> implements DAOPais{

    @Override
    public Pais getPaisByCodigoPais(Long parCodigoPais) {
        return this.buscarPorID(parCodigoPais);
    }

    
}
