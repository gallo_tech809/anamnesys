/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAORecetaPaciente;
import com.gallotech.gallotech.encapsulacion.RecetaPaciente;
/**
 *
 * @author Bayardo Mejia
 */
public class DAOHibRecetaPaciente extends GenericHibernateDAO<RecetaPaciente, Long> implements DAORecetaPaciente{

    @Override
    public RecetaPaciente getRecetaByCodigoReceta(Long parCodigoReceta) {
        return this.buscarPorID(parCodigoReceta);
    }


}