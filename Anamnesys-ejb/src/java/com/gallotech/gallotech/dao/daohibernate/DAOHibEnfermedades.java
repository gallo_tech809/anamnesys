/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOEnfermedades;
import com.gallotech.gallotech.encapsulacion.Enfermedades;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibEnfermedades extends GenericHibernateDAO<Enfermedades, Long> implements DAOEnfermedades {

    @Override
    public Enfermedades getEnfermedadesByCodigoEnfermedades(Long parCodigoEnfermedades) {
        return this.buscarPorID(parCodigoEnfermedades);
    }
    
}
