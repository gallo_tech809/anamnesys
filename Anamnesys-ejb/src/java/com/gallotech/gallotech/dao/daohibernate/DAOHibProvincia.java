/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOProvincia;
import com.gallotech.gallotech.encapsulacion.Provincia;
/**
 *
 * @author Bayardo Mejia
 */
public class DAOHibProvincia extends GenericHibernateDAO<Provincia, Long> implements DAOProvincia{

    @Override
    public Provincia getProvinciaByCodigoProvincia(Long parCodigoProvincia) {
        return this.buscarPorID(parCodigoProvincia);
    }


    
}
