/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAODoctores;
import com.gallotech.gallotech.encapsulacion.Doctores;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibDoctores extends GenericHibernateDAO<Doctores, Long> implements DAODoctores {

    @Override
    public Doctores getDoctoresByCodigoDoctores(Long parCodigoDocoteres) {
        return this.buscarPorID(parCodigoDocoteres);
    }
    
}
