/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOUsuarios;
import com.gallotech.gallotech.encapsulacion.Usuarios;

/**
 *
 * @author alvasrey
 */
public class DAOHibUsuarios extends GenericHibernateDAO<Usuarios, Long> implements DAOUsuarios {

    @Override
    public Usuarios getUsuarioByCodigoUsuario(Long parCodigoUsuario) {
       return this.buscarPorID(parCodigoUsuario);
    }
    
}
