/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOEstudiosClinicos;
import com.gallotech.gallotech.encapsulacion.EstudiosClinicos;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibEstudiosClinicos extends GenericHibernateDAO<EstudiosClinicos, Long> implements DAOEstudiosClinicos {

    @Override
    public EstudiosClinicos getEstudiosClinicosByCodigoEstudiosClinicos(Long parEstudiosClinicos) {
        return this.buscarPorID(parEstudiosClinicos);
    }
    
}
