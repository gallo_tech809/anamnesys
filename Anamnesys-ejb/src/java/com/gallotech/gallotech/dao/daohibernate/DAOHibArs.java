/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOArs;
import com.gallotech.gallotech.encapsulacion.Ars;
import java.io.Serializable;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibArs extends GenericHibernateDAO<Ars, Long> implements DAOArs {

    @Override
    public Ars getArsByCodigoArs(Long parCodigoArs) {
       return this.buscarPorID(parCodigoArs);
    }
    
}
