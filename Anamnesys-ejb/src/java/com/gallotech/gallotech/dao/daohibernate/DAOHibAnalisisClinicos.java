/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOAnalisisClinicos;
import com.gallotech.gallotech.encapsulacion.AnalisisClinicos;
import java.io.Serializable;

/**
 *
 * @author HiraldoTran
 */
public class DAOHibAnalisisClinicos extends GenericHibernateDAO<AnalisisClinicos, Long> implements DAOAnalisisClinicos {

    @Override
    public AnalisisClinicos getAnalisisClinicoBycodigoAnalisisClinico(Long parcodigoAnalisisClinico) {
       return this.buscarPorID(parcodigoAnalisisClinico);
    }
    
}
