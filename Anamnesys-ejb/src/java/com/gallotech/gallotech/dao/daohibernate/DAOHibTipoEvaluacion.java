/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOTipoEvaluacion;
import com.gallotech.gallotech.encapsulacion.TipoEvaluacion;
/**
 *
 * @author Bayardo Mejia
 */
public class DAOHibTipoEvaluacion extends GenericHibernateDAO<TipoEvaluacion, Long> implements DAOTipoEvaluacion{

    @Override
    public TipoEvaluacion getTipoEvaluacionByCodigoTipoEvaluacion(Long parCodigoTipoEvaluacion) {
        return this.buscarPorID(parCodigoTipoEvaluacion);
    }
    
}
