/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.daohibernate;

/**
 *
 * @author Bayardo Mejia
 */
import com.gallotech.gallotech.dao.genericos.GenericHibernateDAO;
import com.gallotech.gallotech.dao.interfaces.DAOMedicamentos;
import com.gallotech.gallotech.encapsulacion.Medicamentos;

public class DAOHibMedicamentos extends GenericHibernateDAO<Medicamentos, Long> implements DAOMedicamentos{

    @Override
    public Medicamentos getMedicamentoByCodigoMedicamento(Long parCodigoMedicamento) {
        return this.buscarPorID(parCodigoMedicamento);
    }



    
}
