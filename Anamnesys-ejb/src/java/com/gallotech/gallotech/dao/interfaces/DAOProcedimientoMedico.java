/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.dao.interfaces;

import com.gallotech.gallotech.dao.genericos.GenericDAO;
import com.gallotech.gallotech.encapsulacion.ProcedimientoMedico;

/**
 *
 * @author HiraldoTran
 */
public interface DAOProcedimientoMedico extends GenericDAO<ProcedimientoMedico, Long> {
    
}
