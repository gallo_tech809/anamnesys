/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gallotech.gallotech.controlador;

import com.gallotech.gallotech.utileria.helpers.HelperDePermisos;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import lombok.Getter;

/*
 *
 * @author jrozon
 */
@ManagedBean(name = "navigationBean")
@Named(value = "navigationBean")
@RequestScoped
public class NavigationBean {

    @Getter
    String baseURL = "http://localhost:8080/uiBuild/";
    @Getter
    String contextURLSistema = "/GalloTech_Anamnesys/faces/app/Anamnesys";
    @Getter
    String commonURL = "/GalloTech_Anamnesys/faces/app/Comun";
    @Getter
    String logoutURL = "/GalloTech_Anamnesys";
    @Getter
    String loginURL = "/GalloTech_Anamnesys";
    @Getter
    Integer codigoSistema = 1;

    public NavigationBean() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, Object> sessionMap = fc.getExternalContext().getSessionMap();
        Object codigoSistema = sessionMap.get("codigoSistema");
        Integer codigoSistemaEjecutado = 0;
        if (codigoSistema != null && !(codigoSistema + "").trim().isEmpty()) {
            codigoSistemaEjecutado = Integer.valueOf(codigoSistema + "");
        }
        asignarURLSistema(codigoSistemaEjecutado);
    }

    public NavigationBean(Integer parCodigoSistema) {
        this.codigoSistema = parCodigoSistema;
        asignarURLSistema(parCodigoSistema);
    }

    public String toIndex() {
        return loginURL + "faces/index.xhtml";
    }

    public String toAplicaciones() {
        return commonURL + "/Landing/Aplicaciones.xhtml";
    }

    public String toNewPass() {
        return "/app/Comun/Ventanas/cambiarPass.xhtml";
    }

    public String toProductos() {
        return contextURLSistema + "/productos.xhtml";
    }

    public String toHistorial() {
        return contextURLSistema + "/historial.xhtml";
    }
    
    public String toIndexProductos() {
        return contextURLSistema + "/index.xhtml";
    }

    public String toDirecciones() {
        return contextURLSistema + "/direcciones.xhtml";
    }

    public String toCheckout() {
        return contextURLSistema + "/checkout.xhtml";
    }

    public String toLogin() {
        return loginURL;
    }

    public String toLogout() {
        return logoutURL;
    }

    public void asignarURLSistema(Integer parCodigoSistema) {
        System.out.println("ASIGNANDO URL AL SISTEMA: sistema= " + parCodigoSistema);
        
        this.codigoSistema = parCodigoSistema;
    }

    public void doLogout() {

        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpSession sesion = (HttpSession) facesContext.getExternalContext().getSession(false);
        sesion.invalidate(); // Elimina la sesion 
        toLogout();

    }

       // Esta función es igual que la de arriba ya que sin importar la aplicación te llevará al index
    public String toAppIndex() {
        HelperDePermisos permisos = new HelperDePermisos();
        System.out.println("CODIGO_SISTEMA : toAppIndex()" + codigoSistema);
        permisos.agregarSistemaAccedido(Short.parseShort(codigoSistema + ""));
        System.out.println("RUTA : " + contextURLSistema);
        return contextURLSistema + "/index.xhtml";
    }

    public String toRegistrar() {
        System.out.println("to registar");
        return "/Newsoft_Portal_Aspirante_Web/faces/registar.xhtml";
    }

    public String toUserInfo() {
        return commonURL + "/Config/userConfig.xhtml";
    }

    public String toAyuda() {
        return commonURL + "/Ayuda/Ayuda.xhtml";
    }

    public String toAutor() {
        return commonURL + "/Ayuda/Autor.xhtml";
    }

    public String getLogoutURL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
